package com.dolnikova.tm.api;

import com.dolnikova.tm.command.AbstractCommand;
import com.dolnikova.tm.endpoint.*;
import org.jetbrains.annotations.Nullable;

import javax.enterprise.inject.Alternative;
import javax.enterprise.inject.Any;
import java.util.Collection;
import java.util.Scanner;

public interface ServiceLocator {
    @Nullable Collection<AbstractCommand> getCommands();
    @Nullable TaskEndpoint getTaskEndpoint();
    @Nullable ProjectEndpoint getProjectEndpoint();
    @Nullable UserEndpoint getUserEndpoint();
    @Nullable SessionEndpoint getSessionEndpoint();
    @Nullable SessionDTO getSessionDTO();
    void setSessionDTO(final SessionDTO sessionDTO);
    @Nullable UserDTO getUserDTO();
    void setUserDTO(final UserDTO userDTO);
    @Nullable Scanner getScanner();
}
