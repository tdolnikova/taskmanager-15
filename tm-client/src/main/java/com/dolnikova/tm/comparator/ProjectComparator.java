package com.dolnikova.tm.comparator;

import com.dolnikova.tm.endpoint.Project;
import org.jetbrains.annotations.Nullable;

import java.util.Comparator;

public class ProjectComparator {

    @Nullable
    public static Comparator<Project> getStatusComparator() {
        return new Comparator<Project>() {
            @Override
            public int compare(Project o1, Project o2) {
                return o1.getStatus().compareTo(o2.getStatus());
            }
        };
    }

    /*@Nullable
    public static Comparator<Project> getCreationDateComparator() {
        return new Comparator<Project>() {
            @Override
            public int compare(Project o1, Project o2) {
                return o1.getCreationDate().toGregorianCalendar().compareTo
                        (o2.getCreationDate().toGregorianCalendar());
            }
        };
    }*/

    @Nullable
    public static Comparator<Project> getStartDateComparator() {
        return new Comparator<Project>() {
            @Override
            public int compare(Project o1, Project o2) {
                return o1.getDateBegin().toGregorianCalendar().compareTo
                        (o2.getDateBegin().toGregorianCalendar());
            }
        };
    }

    @Nullable
    public static Comparator<Project> getEndDateComparator() {
        return new Comparator<Project>() {
            @Override
            public int compare(Project o1, Project o2) {
                return o1.getDateEnd().toGregorianCalendar().compareTo
                        (o2.getDateEnd().toGregorianCalendar());
            }
        };
    }

}
