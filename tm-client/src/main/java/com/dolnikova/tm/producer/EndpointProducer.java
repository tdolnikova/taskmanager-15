package com.dolnikova.tm.producer;

import com.dolnikova.tm.constant.General;
import com.dolnikova.tm.endpoint.*;

import javax.annotation.Priority;
import javax.enterprise.inject.Default;
import javax.enterprise.inject.Produces;
import javax.inject.Named;
import javax.inject.Singleton;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Scanner;

public class EndpointProducer {

    @Produces
    @Singleton
    public ProjectEndpoint getProjectEndpointConnection() throws MalformedURLException {
        URL projectUrl = new URL(General.ADDRESS + "ProjectEndpoint?WSDL");
        QName projectQname = new QName("http://endpoint.tm.dolnikova.com/", "ProjectEndpointService");
        Service projectService = Service.create(projectUrl, projectQname);
        return projectService.getPort(ProjectEndpoint.class);
    }

    @Produces
    @Singleton
    public TaskEndpoint getTaskEndpointConnection() throws MalformedURLException {
        URL taskUrl = new URL(General.ADDRESS + "TaskEndpoint?WSDL");
        QName taskQname = new QName("http://endpoint.tm.dolnikova.com/", "TaskEndpointService");
        Service taskService = Service.create(taskUrl, taskQname);
        return taskService.getPort(TaskEndpoint.class);
    }

    @Produces
    @Singleton
    public UserEndpoint getUserEndpointConnection() throws MalformedURLException {
        URL userUrl = new URL(General.ADDRESS + "UserEndpoint?WSDL");
        QName userQname = new QName("http://endpoint.tm.dolnikova.com/", "UserEndpointService");
        Service userService = Service.create(userUrl, userQname);
        return userService.getPort(UserEndpoint.class);
    }

    @Produces
    @Singleton
    public SessionEndpoint getSessionEndpointConnection() throws MalformedURLException {
        URL sessionUrl = new URL(General.ADDRESS + "SessionEndpoint?WSDL");
        QName sessionQname = new QName("http://endpoint.tm.dolnikova.com/", "SessionEndpointService");
        Service sessionService = Service.create(sessionUrl, sessionQname);
        return sessionService.getPort(SessionEndpoint.class);
    }

    @Produces
    @Singleton
    @Named("userProducer")
    UserDTO setUserDTO() {
        return new UserDTO();
    }

    @Produces
    @Singleton
    @Named("sessionProducer")
    SessionDTO setSessionDTOO() {
        return new SessionDTO();
    }

    @Produces
    @Singleton
    @Named("scannerProducer")
    public Scanner getScanner() {
        return new Scanner(System.in);
    }

}
