package com.dolnikova.tm.constant;

public class AdditionalMessage {

    public final static String COLON = ": ";
    public final static String INCORRECT_NUMBER = "Некорректный ввод.";
    public final static String TRY_AGAIN = "Попробуйте еще раз.";
    public final static String CHOOSE_SORTING_TYPE = "Выберите вид сортировки";
    public final static String INSERT_TEXT = "Введите текст.";
    public final static String ENTER_FIELD_NAME = "Выберите поле для редактирования:";
    public final static String ENTER = "Введите ";

    public final static String CHOOSE_PROJECT = "Выберите проект.";
    public final static String INSERT_START_DATE = "Введите дату начала.";
    public final static String INSERT_END_DATE = "Введите дату окончания.";
    public final static String PROJECT_NAME = "Найден проект: ";
    public final static String IN_PROJECT = "В проекте ";
    public final static String PROJECT = "Проект";
    public final static String CREATED_M = "создан.";
    public final static String NO_PROJECTS = "Проектов нет.";
    public final static String PROJECT_UPDATED = "Проект обновлен.";
    public final static String PROJECT_DELETED = "Проект удален.";
    public final static String INSERT_PROJECT_NAME = "Введите название проекта.";
    public final static String PROJECT_NAME_DOESNT_EXIST = "Проекта с таким названием не существует.";
    public final static String PROJECT_NAME_ALREADY_EXIST = "Проект с таким названием уже существует.";
    public final static String WHICH_PROJECT_DELETE = "Какой проект хотите удалить?";
    public final static String NO_TASKS_IN_PROJECT = "В выбранном проекте нет задач.";

    public final static String TASK = "Задача";
    public final static String NO_TASKS = "Нет задач.";
    public final static String TASK_ADDITION_COMPLETED = "Создание задач завершено.";
    public final static String CREATED_F = "создана.";
    public final static String INSERT_TASK = "Введите задачу: ";
    public final static String INSERT_NEW_TASK = "Введите новую задачу.";
    public final static String INSERT_TASK_ID = "Введите id задачи.";
    public final static String INSERT_NEW_PROJECT_NAME = "Введите название нового проекта.";
    public final static String TASK_UPDATED = "Задача изменена.";
    public final static String TASK_DELETED = "Задача удалена.";
    public final static String OF_TASKS_WITH_POINT = "задач(и)";

    public final static String REG_ENTER_LOGIN = "Введите логин.";
    public final static String REG_ENTER_PASSWORD = "Введите пароль.";
    public final static String REG_USER_EXISTS = "Пользователь с таким именем существует.";
    public final static String REG_USER_NOT_FOUND = "Пользователь не найден.";
    public final static String REG_USER_REGISTERED = "Пользователь зарегистрирован. Войдите в систему.";
    public final static String REG_USER_SUCCESSFUL_LOGIN = "Вход выполнен.";

    public final static String ENTER_OLD_PASSWORD = "Введите старый пароль.";
    public final static String ENTER_NEW_PASSWORD = "Введить новый пароль.";
    public final static String PASSWORD_CHANGED_SUCCESSFULLY = "Пароль изменен.";
    public final static String WRONG_PASSWORD = "Неверный пароль.";
    public final static String LOGIN_CHANGED = "Логин изменен.";

}
