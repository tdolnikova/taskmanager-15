package com.dolnikova.tm.constant;

public final class Command {

    public final static String HELP = "help";
    public final static String HELP_DESCRIPTION = "show all commands";

    public final static String ABOUT = "about";
    public final static String ABOUT_DESCRIPTION = "show info about the app";

    public final static String FIND_ALL_PROJECTS = "find all projects";
    public final static String FIND_ALL_PROJECTS_DESCRIPTION = "find all projects";
    public final static String FIND_PROJECT = "find project";
    public final static String FIND_PROJECT_DESCRIPTION = "find a project";
    public final static String FIND_PROJECT_BY_NAME = "find project by name";
    public final static String FIND_PROJECT_BY_NAME_DESCRIPTION = "find a project by name";
    public final static String FIND_PROJECT_BY_DESCRIPTION = "find project by desc";
    public final static String FIND_PROJECT_BY_DESCRIPTION_DESCRIPTION = "find a project by description";
    public final static String PERSIST_PROJECT = "create project";
    public final static String PERSIST_PROJECT_DESCRIPTION = "create a project";
    public final static String MERGE_PROJECT = "update project";
    public final static String MERGE_PROJECT_DESCRIPTION = "update a project";
    public final static String REMOVE_PROJECT = "delete project";
    public final static String REMOVE_PROJECT_DESCRIPTION = "delete a project";
    public final static String REMOVE_ALL_PROJECTS = "delete all projects";
    public final static String REMOVE_ALL_PROJECTS_DESCRIPTION = "delete all projects";
    public final static String SORT_PROJECTS = "sort projects";
    public final static String SORT_PROJECTS_DESCRIPTION = "sort projects";

    public final static String FIND_ALL_TASKS = "find all tasks";
    public final static String FIND_ALL_TASKS_DESCRIPTION = "find all tasks";
    public final static String FIND_TASK = "find task";
    public final static String FIND_TASK_DESCRIPTION = "find a task";
    public final static String FIND_TASK_BY_NAME = "find task by name";
    public final static String FIND_TASK_BY_NAME_DESCRIPTION = "find a task by name";
    public final static String FIND_TASK_BY_DESCRIPTION = "find task by desc";
    public final static String FIND_TASK_BY_DESCRIPTION_DESCRIPTION = "find a task by description";
    public final static String PERSIST_TASK = "create task";
    public final static String PERSIST_TASK_DESCRIPTION = "create a task";
    public final static String MERGE_TASK = "update task";
    public final static String MERGE_TASK_DESCRIPTION = "update a task";
    public final static String REMOVE_TASK = "delete task";
    public final static String REMOVE_TASK_DESCRIPTION = "delete a task";
    public final static String REMOVE_ALL_TASKS = "delete all tasks";
    public final static String REMOVE_ALL_TASKS_DESCRIPTION = "delete all tasks";
    public final static String SORT_TASKS = "sort tasks";
    public final static String SORT_TASKS_DESCRIPTION = "sort tasks";

    public final static String EXIT = "exit";
    public final static String EXIT_DESCRIPTION = "exit";

    public final static String USER_AUTH = "auth";
    public final static String USER_AUTH_DESCRIPTION = "authorization";
    public final static String USER_REG = "reg";
    public final static String USER_REG_DESCRIPTION = "registration";
    public final static String USER_CHANGE_PASSWORD = "change password";
    public final static String USER_CHANGE_PASSWORD_DESCRIPTION = "change password";
    public final static String USER_EDIT_PROFILE = "edit profile";
    public final static String USER_EDIT_PROFILE_DESCRIPTION = "edit profile";
    public final static String USER_FIND_PROFILE = "show profile";
    public final static String USER_FIND_PROFILE_DESCRIPTION = "show profile";
    public final static String USER_SIGN_OUT = "sign out";
    public final static String USER_SIGN_OUT_DESCRIPTION = "sign out";

    public final static String DATA_SAVE_FASTERXML_XML = "save faster xml";
    public final static String DATA_SAVE_FASTERXML_XML_DESCRIPTION = "save faster xml";
    public final static String DATA_SAVE_FASTERXML_JSON = "save faster json";
    public final static String DATA_SAVE_FASTERXML_JSON_DESCRIPTION = "save faster json";
    public final static String DATA_SAVE_JAXB_XML = "save jaxb xml";
    public final static String DATA_SAVE_JAXB_XML_DESCRIPTION = "save jaxb xml";
    public final static String DATA_SAVE_JAXB_JSON = "save jaxb json";
    public final static String DATA_SAVE_JAXB_JSON_DESCRIPTION = "save jaxb json";
    public final static String DATA_SAVE_BIN = "save bin";
    public final static String DATA_SAVE_BIN_DESCRIPTION = "save bin";
    public final static String DATA_LOAD_FASTERXML_XML = "load faster xml";
    public final static String DATA_LOAD_FASTERXML_XML_DESCRIPTION = "load faster xml";
    public final static String DATA_LOAD_FASTERXML_JSON = "load faster json";
    public final static String DATA_LOAD_FASTERXML_JSON_DESCRIPTION = "load faster json";
    public final static String DATA_LOAD_JAXB_XML = "load jaxb xml";
    public final static String DATA_LOAD_JAXB_XML_DESCRIPTION = "load jaxb xml";
    public final static String DATA_LOAD_JAXB_JSON = "load jaxb json";
    public final static String DATA_LOAD_JAXB_JSON_DESCRIPTION = "load jaxb json";
    public final static String DATA_LOAD_BIN = "load bin";
    public final static String DATA_LOAD_BIN_DESCRIPTION = "save bin";

}
