package com.dolnikova.tm.command.user;


import com.dolnikova.tm.bootstrap.Bootstrap;
import com.dolnikova.tm.command.AbstractCommand;
import com.dolnikova.tm.constant.AdditionalMessage;
import com.dolnikova.tm.constant.Command;
import com.dolnikova.tm.endpoint.Role;
import com.dolnikova.tm.endpoint.UserDTO;
import com.dolnikova.tm.endpoint.UserEndpoint;
import org.jetbrains.annotations.NotNull;

public final class UserRegisterCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return Command.USER_REG;
    }

    @NotNull
    @Override
    public String description() {
        return Command.USER_REG_DESCRIPTION;
    }

    @Override
    public void execute() throws Exception {
        if (!isSecure()) return;
        System.out.println("[REGISTRATION]");
        System.out.println(AdditionalMessage.REG_ENTER_LOGIN);
        @NotNull String login = "";
        while (login.isEmpty()) {
            login = serviceLocator.getScanner().nextLine();
        }
        System.out.println(AdditionalMessage.REG_ENTER_PASSWORD);
        @NotNull String password = "";
        while (password.isEmpty()) {
            password = serviceLocator.getScanner().nextLine();
        }
        @NotNull final UserDTO newUser = new UserDTO();
        newUser.setLogin(login);
        newUser.setPasswordHash(password);
        newUser.setRole(Role.USER);
        serviceLocator.getUserEndpoint().persistUser(newUser);
        UserDTO registeredUser = serviceLocator.getUserEndpoint().findOneByLoginUser(newUser.getLogin());
        if (registeredUser == null) {
            System.out.println("Пользователь не зарегистрирован");
            return;
        }
        System.out.println("Зарегистрирован пользователь");
        System.out.println(registeredUser.getId());
        System.out.println(registeredUser.getLogin());
        System.out.println(registeredUser.getPasswordHash());
    }

    @Override
    public boolean isSecure() {
        UserDTO currentUser = serviceLocator.getUserDTO();
        if (currentUser == null) return true;
        return currentUser.getId() == null;
    }
}
