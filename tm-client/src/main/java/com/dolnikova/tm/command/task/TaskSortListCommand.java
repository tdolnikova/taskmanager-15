package com.dolnikova.tm.command.task;

import com.dolnikova.tm.bootstrap.Bootstrap;
import com.dolnikova.tm.command.AbstractCommand;
import com.dolnikova.tm.constant.AdditionalMessage;
import com.dolnikova.tm.constant.Command;
import com.dolnikova.tm.endpoint.TaskDTO;
import com.dolnikova.tm.endpoint.UserDTO;
import com.dolnikova.tm.enumerated.SortingType;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public final class TaskSortListCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return Command.SORT_TASKS;
    }

    @NotNull
    @Override
    public String description() {
        return Command.SORT_TASKS_DESCRIPTION;
    }

    @Override
    public void execute() throws Exception {
        if (!isSecure()) return;
        @Nullable final List<TaskDTO> taskList = serviceLocator.getTaskEndpoint().
                findAllTask(serviceLocator.getSessionDTO());
        if (taskList == null || taskList.isEmpty()) {
            System.out.println(AdditionalMessage.NO_PROJECTS);
            return;
        }
        System.out.println(AdditionalMessage.CHOOSE_SORTING_TYPE + ":");
        for(final SortingType type : SortingType.values()) {
            System.out.println(type.displayName());
        }
        boolean typeChosen = false;
        while (!typeChosen) {
            @NotNull final String userChoice = serviceLocator.getScanner().nextLine();
            if (userChoice.isEmpty()) return;
            if (userChoice.equals(SortingType.BY_STATUS.displayName())){
                typeChosen = true;
                //Collections.sort(taskList, TaskComparator.getStatusComparator());
            }
            else if (userChoice.equals(SortingType.BY_CREATION_DATE.displayName())){
                typeChosen = true;
                //Collections.sort(taskList, TaskComparator.getCreationDateComparator());
            }
            else if (userChoice.equals(SortingType.BY_START_DATE.displayName())){
                typeChosen = true;
                //Collections.sort(taskList, TaskComparator.getStartDateComparator());
            }
            else if (userChoice.equals(SortingType.BY_END_DATE.displayName())){
                typeChosen = true;
                //Collections.sort(taskList, TaskComparator.getEndDateComparator());
            }
            else {
                System.out.println(AdditionalMessage.TRY_AGAIN);
            }
        }
        /*for (final Task task : taskList) {
            System.out.println(task.getId());
        }*/
    }

    @Override
    public boolean isSecure() {
        UserDTO currentUser = serviceLocator.getUserDTO();
        if (currentUser == null) return false;
        return currentUser.getId() != null;
    }
}