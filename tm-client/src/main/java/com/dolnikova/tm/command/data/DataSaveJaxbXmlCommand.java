package com.dolnikova.tm.command.data;

import com.dolnikova.tm.command.AbstractCommand;
import com.dolnikova.tm.constant.Command;
import com.dolnikova.tm.endpoint.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.lang.Exception;
import java.util.List;

public final class DataSaveJaxbXmlCommand extends AbstractCommand {

    @Override
    public @NotNull String command() {
        return Command.DATA_SAVE_JAXB_XML;
    }

    @Override
    public @NotNull String description() {
        return Command.DATA_SAVE_JAXB_XML_DESCRIPTION;
    }

    @Override
    public void execute() throws Exception {
        @Nullable final String ownerId = serviceLocator.getUserDTO().getId();
        List<UserDTO> users = serviceLocator.getUserEndpoint().findAllUser(serviceLocator.getSessionDTO());
        List<ProjectDTO> projects = serviceLocator.getProjectEndpoint().findAllProject(serviceLocator.getSessionDTO());
        List<TaskDTO> tasks = serviceLocator.getTaskEndpoint().findAllTask(serviceLocator.getSessionDTO());
        boolean userSaveAllowed = serviceLocator.getUserEndpoint().saveJaxbXmlUser(serviceLocator.getSessionDTO(), serviceLocator.getUserDTO(), users);
        if (userSaveAllowed) System.out.println("ПОЛЬЗОВАТЕЛИ СОХРАНЕНЫ");
        boolean taskSaveAllowed = serviceLocator.getTaskEndpoint().saveJaxbXmlTask(serviceLocator.getSessionDTO(), serviceLocator.getUserDTO(), tasks);
        if (taskSaveAllowed) System.out.println("ЗАДАЧИ СОХРАНЕНЫ");
        boolean projectSaveAllowed = serviceLocator.getProjectEndpoint().saveJaxbXmlProject(serviceLocator.getSessionDTO(), serviceLocator.getUserDTO(), projects);
        if (projectSaveAllowed) System.out.println("ПРОЕКТЫ СОХРАНЕНЫ");
    }

    @Override
    public boolean isSecure() {
        UserDTO currentUser = serviceLocator.getUserDTO();
        if (currentUser == null) return false;
        return currentUser.getId() != null;
    }
}