package com.dolnikova.tm.command.user;

import com.dolnikova.tm.command.AbstractCommand;
import com.dolnikova.tm.constant.AdditionalMessage;
import com.dolnikova.tm.constant.Command;
import com.dolnikova.tm.endpoint.SessionDTO;
import com.dolnikova.tm.endpoint.UserDTO;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class UserAuthCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return Command.USER_AUTH;
    }

    @NotNull
    @Override
    public String description() {
        return Command.USER_AUTH_DESCRIPTION;
    }

    @Override
    public void execute() throws Exception {
        if (!isSecure()) return;
        System.out.println("[AUTHORIZATION]");
        System.out.println(AdditionalMessage.REG_ENTER_LOGIN);
        @NotNull String login = "";
        while (login.isEmpty()) {
            login = serviceLocator.getScanner().nextLine();
        }
        System.out.println(AdditionalMessage.REG_ENTER_PASSWORD);
        @NotNull String password = "";
        while (password.isEmpty()) {
            password = serviceLocator.getScanner().nextLine();
        }
        @Nullable final SessionDTO sessionDTO = serviceLocator.getSessionEndpoint().getSession(login, password);
        if (sessionDTO == null) {
            System.out.println("Не найдена сессия. Отмена.");
            return;
        }
        System.out.println("Получена сессия: " + sessionDTO.getUserId());
        @Nullable final UserDTO currentUser = serviceLocator.getUserEndpoint().findOneBySession(sessionDTO);
        if (currentUser == null) {
            System.out.println("Не найден пользователь. Отмена.");
            return;
        }
        System.out.println("Получен пользователь: " + currentUser.getLogin() + "; id:" + currentUser.getId());
        serviceLocator.setSessionDTO(sessionDTO);
        serviceLocator.setUserDTO(currentUser);
        System.out.println(AdditionalMessage.REG_USER_SUCCESSFUL_LOGIN);
    }

    @Override
    public boolean isSecure() {
        UserDTO currentUser = serviceLocator.getUserDTO();
        if (currentUser == null) return true;
        return currentUser.getId() == null;
    }
}
