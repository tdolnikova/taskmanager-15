package com.dolnikova.tm.command.project;

import com.dolnikova.tm.bootstrap.Bootstrap;
import com.dolnikova.tm.command.AbstractCommand;
import com.dolnikova.tm.constant.AdditionalMessage;
import com.dolnikova.tm.constant.Command;
import com.dolnikova.tm.endpoint.Project;
import com.dolnikova.tm.endpoint.ProjectDTO;
import com.dolnikova.tm.endpoint.UserDTO;
import com.dolnikova.tm.util.DateUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class ProjectPersistCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return Command.PERSIST_PROJECT;
    }

    @NotNull
    @Override
    public String description() {
        return Command.PERSIST_PROJECT_DESCRIPTION;
    }

    @Override
    public void execute() throws Exception {
        if (!isSecure()) return;
        System.out.println(AdditionalMessage.INSERT_NEW_PROJECT_NAME);
        @NotNull final String projectName = serviceLocator.getScanner().nextLine();
        if (projectName.isEmpty()) return;
        @Nullable final ProjectDTO projectDTO =
                serviceLocator.getProjectEndpoint().findOneByNameProject(serviceLocator.getSessionDTO(), projectName);
        if (projectDTO != null) {
            System.out.println(AdditionalMessage.PROJECT_NAME_ALREADY_EXIST);
            return;
        }
        @Nullable final ProjectDTO newProject = new ProjectDTO();
        newProject.setUserId(serviceLocator.getUserDTO().getId());
        newProject.setName(projectName);
        System.out.println(AdditionalMessage.INSERT_START_DATE);
        boolean dateChosen = false;
        while (!dateChosen) {
            @NotNull final String startDate = serviceLocator.getScanner().nextLine();
            if (startDate.isEmpty()) return;
            newProject.setDateBegin(DateUtil.stringToXMLGregorianCalendar(startDate));
            dateChosen = true;
        }
        System.out.println(AdditionalMessage.INSERT_END_DATE);
        dateChosen = false;
        while (!dateChosen) {
            @NotNull final String endDate = serviceLocator.getScanner().nextLine();
            if (endDate.isEmpty()) return;
            newProject.setDateEnd(DateUtil.stringToXMLGregorianCalendar(endDate));
            dateChosen = true;
        }
        serviceLocator.getProjectEndpoint().persistProject(serviceLocator.getSessionDTO(), newProject);
        System.out.println(AdditionalMessage.PROJECT + " " + projectName + " " + AdditionalMessage.CREATED_M);
    }

    @Override
    public boolean isSecure() {
        UserDTO currentUser = serviceLocator.getUserDTO();
        if (currentUser == null) return false;
        return currentUser.getId() != null;
    }
}
