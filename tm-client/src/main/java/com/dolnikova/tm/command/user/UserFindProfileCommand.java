package com.dolnikova.tm.command.user;

import com.dolnikova.tm.command.AbstractCommand;
import com.dolnikova.tm.constant.Command;
import com.dolnikova.tm.endpoint.UserDTO;
import org.jetbrains.annotations.NotNull;

public final class UserFindProfileCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return Command.USER_FIND_PROFILE;
    }

    @NotNull
    @Override
    public String description() {
        return Command.USER_FIND_PROFILE_DESCRIPTION;
    }

    @Override
    public void execute() throws Exception {
        if (!isSecure()) return;
        System.out.println("id: " + serviceLocator.getUserDTO().getId());
        System.out.println("login: " + serviceLocator.getUserDTO().getLogin());
        System.out.println("password: " + serviceLocator.getUserDTO().getPasswordHash());
        System.out.println("role: " + serviceLocator.getUserDTO().getRole());
    }

    @Override
    public boolean isSecure() {
        UserDTO currentUser = serviceLocator.getUserDTO();
        if (currentUser == null) return false;
        return currentUser.getId() != null;
    }

}
