package com.dolnikova.tm.command.project;

import com.dolnikova.tm.bootstrap.Bootstrap;
import com.dolnikova.tm.command.AbstractCommand;
import com.dolnikova.tm.constant.AdditionalMessage;
import com.dolnikova.tm.constant.Command;
import com.dolnikova.tm.endpoint.Project;
import com.dolnikova.tm.endpoint.ProjectDTO;
import com.dolnikova.tm.endpoint.UserDTO;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class ProjectRemoveCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return Command.REMOVE_PROJECT;
    }

    @NotNull
    @Override
    public String description() {
        return Command.REMOVE_PROJECT_DESCRIPTION;
    }

    @Override
    public void execute() throws Exception {
        if (!isSecure()) return;
        if (serviceLocator.getProjectEndpoint().findAllProject(serviceLocator.getSessionDTO()).isEmpty()) {
            System.out.println(AdditionalMessage.NO_PROJECTS);
            return;
        }
        System.out.println(AdditionalMessage.WHICH_PROJECT_DELETE);
        @Nullable ProjectDTO projectDTO = null;
        while (projectDTO == null) {
            @NotNull final String projectName = serviceLocator.getScanner().nextLine();
            if (projectName.isEmpty()) return;
            projectDTO = serviceLocator.getProjectEndpoint().findOneByNameProject(
                    serviceLocator.getSessionDTO(),
                    projectName);
            if (projectDTO != null) serviceLocator.getProjectEndpoint().
                    removeProject(serviceLocator.getSessionDTO(), projectDTO);
            else System.out.println(AdditionalMessage.PROJECT_NAME_DOESNT_EXIST + " " + AdditionalMessage.TRY_AGAIN);
        }
        System.out.println(AdditionalMessage.PROJECT_DELETED);
    }

    @Override
    public boolean isSecure() {
        UserDTO currentUser = serviceLocator.getUserDTO();
        if (currentUser == null) return false;
        return currentUser.getId() != null;
    }
}
