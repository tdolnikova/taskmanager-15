package com.dolnikova.tm.command.task;

import com.dolnikova.tm.bootstrap.Bootstrap;
import com.dolnikova.tm.command.AbstractCommand;
import com.dolnikova.tm.constant.AdditionalMessage;
import com.dolnikova.tm.constant.Command;
import com.dolnikova.tm.constant.General;
import com.dolnikova.tm.endpoint.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.lang.Exception;
import java.util.ArrayList;
import java.util.List;

public final class TaskFindOneCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return Command.FIND_TASK;
    }

    @NotNull
    @Override
    public String description() {
        return Command.FIND_TASK_DESCRIPTION;
    }

    @Override
    public void execute() throws Exception {
        if (!isSecure()) return;
        @Nullable final ProjectDTO project = findProject();
        if (project == null) return;
        @Nullable final List<TaskDTO> tasks = serviceLocator.getTaskEndpoint().
                findAllTask(serviceLocator.getSessionDTO());
        if (tasks == null || tasks.isEmpty()) {
            System.out.println(AdditionalMessage.NO_TASKS);
            return;
        }
        @Nullable final List<TaskDTO> projectTasks = new ArrayList<>();
        for (TaskDTO task : tasks) {
            if (task.getProjectId().equals(project.getId()) && task.getUserId().equals(serviceLocator.getUserDTO().getId()))
                projectTasks.add(task);
        }
        if (projectTasks.isEmpty()) {
            System.out.println(AdditionalMessage.NO_TASKS_IN_PROJECT);
            return;
        }
        System.out.println(AdditionalMessage.IN_PROJECT + project.getName() + " " + projectTasks.size() + " " + AdditionalMessage.OF_TASKS_WITH_POINT + ":");
        for (final TaskDTO task : projectTasks) {
            System.out.println(task.getId() + " " + task.getName());
        }
        System.out.println(AdditionalMessage.INSERT_TASK_ID);
        @Nullable TaskDTO task = null;
        while (task == null) {
            @NotNull final String taskId = serviceLocator.getScanner().nextLine();
            if (taskId.isEmpty()) break;
            task = serviceLocator.getTaskEndpoint().
                    findOneByIdTask(serviceLocator.getSessionDTO(), taskId);
            if (task == null) System.out.println(AdditionalMessage.INCORRECT_NUMBER);
            else System.out.println(General.ID + AdditionalMessage.COLON + taskId + " " + task.getName());
        }

    }

    @Nullable
    private ProjectDTO findProject() {
        if (serviceLocator.getTaskEndpoint().findAllTask(serviceLocator.getSessionDTO()).isEmpty()) {
            System.out.println(AdditionalMessage.NO_TASKS);
            return null;
        }
        if (serviceLocator.getProjectEndpoint().findAllProject(serviceLocator.getSessionDTO()).isEmpty()) {
            System.out.println(AdditionalMessage.NO_PROJECTS);
            return null;
        }
        System.out.println(AdditionalMessage.CHOOSE_PROJECT);
        @Nullable ProjectDTO project = null;
        while (project == null) {
            @NotNull final String projectName = serviceLocator.getScanner().nextLine();
            if (projectName.isEmpty()) break;
            project = serviceLocator.getProjectEndpoint().
                    findOneByNameProject(serviceLocator.getSessionDTO(), projectName);
            if (project == null) System.out.println(AdditionalMessage.PROJECT_NAME_DOESNT_EXIST + " " + AdditionalMessage.TRY_AGAIN);
        }
        return project;
    }

    @Override
    public boolean isSecure() {
        UserDTO currentUser = serviceLocator.getUserDTO();
        if (currentUser == null) return false;
        return currentUser.getId() != null;
    }

}
