package com.dolnikova.tm.command.project;

import com.dolnikova.tm.bootstrap.Bootstrap;
import com.dolnikova.tm.command.AbstractCommand;
import com.dolnikova.tm.constant.AdditionalMessage;
import com.dolnikova.tm.constant.Command;
import com.dolnikova.tm.endpoint.Project;
import com.dolnikova.tm.endpoint.ProjectDTO;
import com.dolnikova.tm.endpoint.UserDTO;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public final class ProjectFindByNameCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return Command.FIND_PROJECT_BY_NAME;
    }

    @NotNull
    @Override
    public String description() {
        return Command.FIND_PROJECT_BY_NAME_DESCRIPTION;
    }

    @Override
    public void execute() throws Exception {
        if (!isSecure()) return;
        @NotNull String userInput = "";
        System.out.println(AdditionalMessage.INSERT_TEXT);
        while (userInput.isEmpty()) {
            userInput = serviceLocator.getScanner().nextLine();
        }
        @Nullable final List<ProjectDTO> projectList = serviceLocator.getProjectEndpoint().
                findAllByNameProject(serviceLocator.getSessionDTO(), userInput);
        for (final ProjectDTO projectDTO : projectList) {
            System.out.println(projectDTO.getName());
        }
    }

    @Override
    public boolean isSecure() {
        UserDTO currentUser = serviceLocator.getUserDTO();
        if (currentUser == null) return false;
        return currentUser.getId() != null;
    }
}
