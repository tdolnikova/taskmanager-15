package com.dolnikova.tm.command.data;

import com.dolnikova.tm.command.AbstractCommand;
import com.dolnikova.tm.constant.Command;
import com.dolnikova.tm.endpoint.*;
import org.jetbrains.annotations.NotNull;

import java.lang.Exception;
import java.util.List;

public class DataLoadJaxbJsonCommand extends AbstractCommand {

    @Override
    public @NotNull String command() {
        return Command.DATA_LOAD_JAXB_JSON;
    }

    @Override
    public @NotNull String description() {
        return Command.DATA_LOAD_JAXB_JSON_DESCRIPTION;
    }

    @Override
    public void execute() throws Exception {
        List<ProjectDTO> projects = serviceLocator.getProjectEndpoint().loadJaxbJsonProject(serviceLocator.getSessionDTO(), serviceLocator.getUserDTO());
        List<TaskDTO> tasks = serviceLocator.getTaskEndpoint().loadJaxbJsonTask(serviceLocator.getSessionDTO(), serviceLocator.getUserDTO());
        List<UserDTO> users = serviceLocator.getUserEndpoint().loadJaxbJsonUser(serviceLocator.getSessionDTO(), serviceLocator.getUserDTO());
        if (projects == null
                || tasks == null
                || users == null) {
            throw new IllegalAccessException();
        }
        System.out.println("ЗАГРУЖЕНО");
    }

    @Override
    public boolean isSecure() {
        UserDTO currentUser = serviceLocator.getUserDTO();
        if (currentUser == null) return false;
        return currentUser.getId() != null;
    }
}