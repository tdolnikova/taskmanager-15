package com.dolnikova.tm.command.project;

import com.dolnikova.tm.command.AbstractCommand;
import com.dolnikova.tm.constant.Command;
import com.dolnikova.tm.endpoint.UserDTO;
import org.jetbrains.annotations.NotNull;

public final class ProjectRemoveAllCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return Command.REMOVE_ALL_PROJECTS;
    }

    @NotNull
    @Override
    public String description() {
        return Command.REMOVE_ALL_PROJECTS_DESCRIPTION;
    }

    @Override
    public void execute() throws Exception {
        if (!isSecure()) return;
        serviceLocator.getProjectEndpoint().removeAllProject(serviceLocator.getSessionDTO());
    }

    @Override
    public boolean isSecure() {
        UserDTO currentUser = serviceLocator.getUserDTO();
        if (currentUser == null) return false;
        return currentUser.getId() != null;
    }
}
