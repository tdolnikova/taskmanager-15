package com.dolnikova.tm.command.project;

import com.dolnikova.tm.command.AbstractCommand;
import com.dolnikova.tm.constant.AdditionalMessage;
import com.dolnikova.tm.constant.Command;
import com.dolnikova.tm.endpoint.Project;
import com.dolnikova.tm.endpoint.ProjectDTO;
import com.dolnikova.tm.endpoint.UserDTO;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public final class ProjectFindAllCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return Command.FIND_ALL_PROJECTS;
    }

    @NotNull
    @Override
    public String description() {
        return Command.FIND_ALL_PROJECTS_DESCRIPTION;
    }

    @Override
    public void execute() {
        if (!isSecure()) return;
        System.out.println("[PROJECT LIST]");
        @Nullable final List<ProjectDTO> allProjects = serviceLocator.getProjectEndpoint().
                findAllProject(serviceLocator.getSessionDTO());
        if (allProjects == null || allProjects.isEmpty()) System.out.println(AdditionalMessage.NO_PROJECTS);
        else {
            for (final ProjectDTO projectDTO : allProjects) {
                System.out.println(projectDTO.getId());
            }
        }
    }

    @Override
    public boolean isSecure() {
        UserDTO currentUser = serviceLocator.getUserDTO();
        if (currentUser == null) return false;
        return currentUser.getId() != null;
    }

}
