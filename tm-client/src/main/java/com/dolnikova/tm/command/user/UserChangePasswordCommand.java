package com.dolnikova.tm.command.user;

import com.dolnikova.tm.command.AbstractCommand;
import com.dolnikova.tm.constant.AdditionalMessage;
import com.dolnikova.tm.constant.Command;
import com.dolnikova.tm.endpoint.DataType;
import com.dolnikova.tm.endpoint.UserDTO;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class UserChangePasswordCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return Command.USER_CHANGE_PASSWORD;
    }

    @NotNull
    @Override
    public String description() {
        return Command.USER_CHANGE_PASSWORD_DESCRIPTION;
    }

    @Override
    public void execute() throws Exception {
        if (!isSecure()) return;
        System.out.println(AdditionalMessage.ENTER_OLD_PASSWORD);
        @NotNull String oldPassword = "";
        while (oldPassword.isEmpty()) {
            oldPassword = serviceLocator.getScanner().nextLine();
        }
        boolean passwordCorrect = serviceLocator.getUserEndpoint().
                checkPassword(serviceLocator.getSessionDTO(), oldPassword);
        if (!passwordCorrect) {
            System.out.println(AdditionalMessage.WRONG_PASSWORD);
            return;
        }
        System.out.println(AdditionalMessage.ENTER_NEW_PASSWORD);
        @NotNull String newPassword = "";
        while (newPassword.isEmpty()) {
            newPassword = serviceLocator.getScanner().nextLine();
        }
        serviceLocator.getUserEndpoint().mergeUser(
                serviceLocator.getSessionDTO(),
                newPassword,
                serviceLocator.getUserDTO(),
                DataType.PASSWORD);
        System.out.println(AdditionalMessage.PASSWORD_CHANGED_SUCCESSFULLY);
        @Nullable final UserDTO updatedUser = serviceLocator.getUserEndpoint().
                findOneBySession(serviceLocator.getSessionDTO());
        if (updatedUser != null) serviceLocator.setUserDTO(updatedUser);
    }

    @Override
    public boolean isSecure() {
        UserDTO currentUser = serviceLocator.getUserDTO();
        if (currentUser == null) return false;
        return currentUser.getId() != null;
    }
}
