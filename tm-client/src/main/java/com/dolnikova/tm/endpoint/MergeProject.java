
package com.dolnikova.tm.endpoint;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for mergeProject complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="mergeProject"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="session" type="{http://endpoint.tm.dolnikova.com/}sessionDTO" minOccurs="0"/&gt;
 *         &lt;element name="newData" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="entityToMerge" type="{http://endpoint.tm.dolnikova.com/}projectDTO" minOccurs="0"/&gt;
 *         &lt;element name="dataType" type="{http://endpoint.tm.dolnikova.com/}dataType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "mergeProject", propOrder = {
    "session",
    "newData",
    "entityToMerge",
    "dataType"
})
public class MergeProject {

    protected SessionDTO session;
    protected String newData;
    protected ProjectDTO entityToMerge;
    @XmlSchemaType(name = "string")
    protected DataType dataType;

    /**
     * Gets the value of the session property.
     * 
     * @return
     *     possible object is
     *     {@link SessionDTO }
     *     
     */
    public SessionDTO getSession() {
        return session;
    }

    /**
     * Sets the value of the session property.
     * 
     * @param value
     *     allowed object is
     *     {@link SessionDTO }
     *     
     */
    public void setSession(SessionDTO value) {
        this.session = value;
    }

    /**
     * Gets the value of the newData property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNewData() {
        return newData;
    }

    /**
     * Sets the value of the newData property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNewData(String value) {
        this.newData = value;
    }

    /**
     * Gets the value of the entityToMerge property.
     * 
     * @return
     *     possible object is
     *     {@link ProjectDTO }
     *     
     */
    public ProjectDTO getEntityToMerge() {
        return entityToMerge;
    }

    /**
     * Sets the value of the entityToMerge property.
     * 
     * @param value
     *     allowed object is
     *     {@link ProjectDTO }
     *     
     */
    public void setEntityToMerge(ProjectDTO value) {
        this.entityToMerge = value;
    }

    /**
     * Gets the value of the dataType property.
     * 
     * @return
     *     possible object is
     *     {@link DataType }
     *     
     */
    public DataType getDataType() {
        return dataType;
    }

    /**
     * Sets the value of the dataType property.
     * 
     * @param value
     *     allowed object is
     *     {@link DataType }
     *     
     */
    public void setDataType(DataType value) {
        this.dataType = value;
    }

}
