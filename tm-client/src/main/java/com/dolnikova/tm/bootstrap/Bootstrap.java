package com.dolnikova.tm.bootstrap;

import com.dolnikova.tm.api.ServiceLocator;
import com.dolnikova.tm.command.AbstractCommand;
import com.dolnikova.tm.constant.General;
import com.dolnikova.tm.endpoint.*;
import com.dolnikova.tm.exception.CommandCorruptException;
import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import java.lang.Exception;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.*;

@Getter
@Setter
@Singleton
public final class Bootstrap implements ServiceLocator {

    @NotNull
    private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();
    @NotNull
    private final Set<Class<? extends AbstractCommand>> classes = new Reflections("com.dolnikova.tm").getSubTypesOf(AbstractCommand.class);

    @Inject
    @Nullable
    @Named("sessionProducer")
    private SessionDTO sessionDTO;
    @Inject
    @Nullable
    @Named("userProducer")
    private UserDTO userDTO;
    @Inject
    @Nullable
    @Named("scannerProducer")
    private Scanner scanner;
    @Inject
    @Nullable
    private ProjectEndpoint projectEndpoint;
    @Inject
    @Nullable
    private TaskEndpoint taskEndpoint;
    @Inject
    @Nullable
    private UserEndpoint userEndpoint;
    @Inject
    @Nullable
    private SessionEndpoint sessionEndpoint;


    public void init(@NotNull Class[] CLASSES) {
        try {
            for (@NotNull final Class clazz : CLASSES) {
                if (!AbstractCommand.class.isAssignableFrom(clazz)) continue;
                registryCommand((AbstractCommand) clazz.newInstance());
            }
            connect();
            start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void registryCommand(@NotNull final AbstractCommand command) throws CommandCorruptException {
        @Nullable final String cliCommand = command.command();
        @Nullable final String cliDescription = command.description();
        if (cliCommand.isEmpty())
            throw new CommandCorruptException();
        if (cliDescription.isEmpty())
            throw new CommandCorruptException();
        command.setServiceLocator(this);
        commands.put(cliCommand, command);
    }

    private void connect() throws MalformedURLException {
        URL taskUrl = new URL(General.ADDRESS + "TaskEndpoint?WSDL");
        QName taskQname = new QName("http://endpoint.tm.dolnikova.com/", "TaskEndpointService");
        Service taskService = Service.create(taskUrl, taskQname);
        taskEndpoint = taskService.getPort(TaskEndpoint.class);

        URL projectUrl = new URL(General.ADDRESS + "ProjectEndpoint?WSDL");
        QName projectQname = new QName("http://endpoint.tm.dolnikova.com/", "ProjectEndpointService");
        Service projectService = Service.create(projectUrl, projectQname);
        projectEndpoint = projectService.getPort(ProjectEndpoint.class);

        URL userUrl = new URL(General.ADDRESS + "UserEndpoint?WSDL");
        QName userQname = new QName("http://endpoint.tm.dolnikova.com/", "UserEndpointService");
        Service userService = Service.create(userUrl, userQname);
        userEndpoint = userService.getPort(UserEndpoint.class);

        URL sessionUrl = new URL(General.ADDRESS + "SessionEndpoint?WSDL");
        QName sessionQname = new QName("http://endpoint.tm.dolnikova.com/", "SessionEndpointService");
        Service sessionService = Service.create(sessionUrl, sessionQname);
        sessionEndpoint = sessionService.getPort(SessionEndpoint.class);
    }

    private void start() {
        System.out.println("*** WELCOME TO TASK MANAGER ***");
        if (getSessionDTO() != null) System.out.println("Current session: " + getSessionDTO().getId());
        @NotNull String command = "";
        while (!"exit".equals(command)) {
            try {
                command = scanner.nextLine();
                if (command.isEmpty()) continue;
                if (commands.get(command) == null) throw new CommandCorruptException();
                // проверка сессии
                execute(command);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void execute(final String command) throws Exception {
        if (command == null || command.isEmpty()) return;
        @Nullable final AbstractCommand abstractCommand = commands.get(command);
        if (abstractCommand == null) return;
        abstractCommand.execute();
    }

    @NotNull
    public Collection<AbstractCommand> getCommands() {
        return commands.values();
    }

}