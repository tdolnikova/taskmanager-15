package com.dolnikova.tm;

import com.dolnikova.tm.constant.General;
import com.dolnikova.tm.endpoint.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

public class SessionEndpointTest {

    @Nullable
    private UserEndpoint userEndpoint;
    @Nullable
    private SessionEndpoint sessionEndpoint;
    @NotNull
    private SessionDTO sessionDTO;
    @NotNull
    private UserDTO user = new UserDTO();

    @Before
    public void startTest() throws MalformedURLException {
        System.out.println("[START]");
        URL userUrl = new URL(General.ADDRESS + "UserEndpoint?WSDL");
        QName userQname = new QName("http://endpoint.tm.dolnikova.com/", "UserEndpointService");
        Service userService = Service.create(userUrl, userQname);
        userEndpoint = userService.getPort(UserEndpoint.class);

        URL sessionUrl = new URL(General.ADDRESS + "SessionEndpoint?WSDL");
        QName sessionQname = new QName("http://endpoint.tm.dolnikova.com/", "SessionEndpointService");
        Service sessionService = Service.create(sessionUrl, sessionQname);
        sessionEndpoint = sessionService.getPort(SessionEndpoint.class);

        assert userEndpoint != null;
        user = userEndpoint.findOneByLoginUser("user");
        assert sessionEndpoint != null;
        sessionDTO = sessionEndpoint.getSession("user", "user");
    }

    @After
    public void finishTest() {
        System.out.println("[FINISH]");
    }

    @Test
    public void findOneById() {
        assert sessionEndpoint != null;
        sessionEndpoint.findOneByIdSession(user.getId(), sessionDTO.getId());
    }

    @Test
    public void findOneByUserId() {
        assert sessionEndpoint != null;
        sessionEndpoint.findOneByUserIdSession(user.getId());
    }

    @Test
    public void findOneBySignature() {
        assert sessionEndpoint != null;
        sessionEndpoint.findOneBySignature(sessionDTO.getSignature());
    }

    @Test
    public void findAll() {
        assert sessionEndpoint != null;
        sessionEndpoint.findAllSession(user.getId());
    }

    @Test
    public void persist() {
        SessionDTO s = new SessionDTO();
        s.setUserId(user.getId());
        s.setId("1");
        assert sessionEndpoint != null;
        sessionEndpoint.persistSession(s, user);
        @Nullable final SessionDTO savedSession = sessionEndpoint.findOneByIdSession(user.getId(), s.getId());
        Assert.assertNotNull(savedSession);
        System.out.println(savedSession.getId());
        sessionEndpoint.removeSession(savedSession, user);
        @Nullable final SessionDTO deletedSession = sessionEndpoint.findOneByIdSession(user.getId(), "1");
        Assert.assertNull(deletedSession);
    }

}
