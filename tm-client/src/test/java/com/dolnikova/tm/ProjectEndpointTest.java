package com.dolnikova.tm;

import com.dolnikova.tm.constant.General;
import com.dolnikova.tm.endpoint.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

public class ProjectEndpointTest {

    @Nullable
    private UserEndpoint userEndpoint;
    @Nullable
    private ProjectEndpoint projectEndpoint;
    @Nullable
    private SessionEndpoint sessionEndpoint;
    @Nullable
    private SessionDTO sessionDTO;
    @NotNull
    private UserDTO user = new UserDTO();
    @Nullable
    private ProjectDTO test;

    @Before
    public void startTest() throws MalformedURLException {
        System.out.println("[START]");
        URL userUrl = new URL(General.ADDRESS + "UserEndpoint?WSDL");
        QName userQname = new QName("http://endpoint.tm.dolnikova.com/", "UserEndpointService");
        Service userService = Service.create(userUrl, userQname);
        userEndpoint = userService.getPort(UserEndpoint.class);

        URL projectUrl = new URL(General.ADDRESS + "ProjectEndpoint?WSDL");
        QName projectQname = new QName("http://endpoint.tm.dolnikova.com/", "ProjectEndpointService");
        Service projectService = Service.create(projectUrl, projectQname);
        projectEndpoint = projectService.getPort(ProjectEndpoint.class);

        URL sessionUrl = new URL(General.ADDRESS + "SessionEndpoint?WSDL");
        QName sessionQname = new QName("http://endpoint.tm.dolnikova.com/", "SessionEndpointService");
        Service sessionService = Service.create(sessionUrl, sessionQname);
        sessionEndpoint = sessionService.getPort(SessionEndpoint.class);

        assert userEndpoint != null;
        user = userEndpoint.findOneByLoginUser("user");
        assert sessionEndpoint != null;
        sessionDTO = sessionEndpoint.getSession("user", "user");
        assert projectEndpoint != null;
        test = projectEndpoint.findOneByNameProject(sessionDTO, "test");
    }

    @After
    public void finishTest() {
        System.out.println("[FINISH]");
    }

    @Test
    public void findOneByIdProject() {
        assert projectEndpoint != null;
        assert test != null;
        ProjectDTO p = projectEndpoint.findOneByIdProject(sessionDTO, test.getId());
        Assert.assertNotNull(p);
        Assert.assertEquals(p.getName(), test.getName());
    }

    @Test
    public void findAllProject() {
        assert projectEndpoint != null;
        List<ProjectDTO> list = projectEndpoint.findAllProject(sessionDTO);
        Assert.assertNotNull(list);
    }

    @Test
    public void findAllByNameProject() {
        assert projectEndpoint != null;
        List<ProjectDTO> list = projectEndpoint.findAllByNameProject(sessionDTO, "test");
        Assert.assertNotNull(list);
    }

    @Test
    public void persistProject() {
        ProjectDTO testProject = new ProjectDTO();
        testProject.setName("p-test");
        testProject.setUserId(user.getId());
        testProject.setDescription("p-test");
        assert projectEndpoint != null;
        projectEndpoint.persistProject(sessionDTO, testProject);
        ProjectDTO p = projectEndpoint.findOneByNameProject(sessionDTO, "p-test");
        Assert.assertEquals(p.getName(), testProject.getName());
        projectEndpoint.removeProject(sessionDTO, p);
    }

    @Test
    public void mergeProject() {
        assert projectEndpoint != null;
        projectEndpoint.mergeProject(sessionDTO, "new-project-name", test, DataType.NAME);
        ProjectDTO p = projectEndpoint.findOneByNameProject(sessionDTO, "new-project-name");
        Assert.assertNotNull(p);
        projectEndpoint.mergeProject(sessionDTO, "test", p, DataType.NAME);
    }

}
