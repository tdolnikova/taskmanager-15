package com.dolnikova.tm;

import com.dolnikova.tm.constant.General;
import com.dolnikova.tm.endpoint.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import java.net.MalformedURLException;
import java.net.URL;

public class UserEndpointTest {
    @Nullable
    private UserEndpoint userEndpoint;
    @Nullable
    private SessionEndpoint sessionEndpoint;
    @Nullable
    private SessionDTO sessionDTO;
    @Nullable
    private SessionDTO session;

    @Before
    public void startTest() throws MalformedURLException {
        System.out.println("[START]");
        URL userUrl = new URL(General.ADDRESS + "UserEndpoint?WSDL");
        QName userQname = new QName("http://endpoint.tm.dolnikova.com/", "UserEndpointService");
        Service userService = Service.create(userUrl, userQname);
        userEndpoint = userService.getPort(UserEndpoint.class);

        URL sessionUrl = new URL(General.ADDRESS + "SessionEndpoint?WSDL");
        QName sessionQname = new QName("http://endpoint.tm.dolnikova.com/", "SessionEndpointService");
        Service sessionService = Service.create(sessionUrl, sessionQname);
        sessionEndpoint = sessionService.getPort(SessionEndpoint.class);

        assert sessionEndpoint != null;
        sessionDTO = sessionEndpoint.getSession("user", "user");
    }

    @After
    public void finishTest() {
        assert userEndpoint != null;
        System.out.println("[FINISH]");
    }

    @Test
    public void findUserByLogin() {
        assert userEndpoint != null;
        UserDTO test = userEndpoint.findOneByLoginUser("user");
        Assert.assertNotNull(test);
    }

    @Test
    public void findAllByLogin() {
        assert userEndpoint != null;
        UserDTO test = userEndpoint.findOneByLoginUser("user");
        Assert.assertNotNull(test);
    }

    @Test
    public void addUser() {
        @NotNull final UserDTO newUser = new UserDTO();
        newUser.setLogin("test");
        newUser.setPasswordHash("test");
        assert userEndpoint != null;
        userEndpoint.persistUser(newUser);
        assert sessionEndpoint != null;
        session = sessionEndpoint.getSession("test", "test");
        Assert.assertNotNull(session);
        @NotNull UserDTO u = userEndpoint.findOneByLoginUser("test");
        Assert.assertNotNull(u);
        userEndpoint.removeUser(session, u);
        u = userEndpoint.findOneByLoginUser("test");
        Assert.assertNull(u);
    }

    @Test
    public void updateUser() {
        assert userEndpoint != null;
        @NotNull final UserDTO user = userEndpoint.findOneByLoginUser("user");
        @NotNull final String newLogin = "merge-test";
        assert userEndpoint != null;
        userEndpoint.mergeUser(sessionDTO, newLogin, user, DataType.LOGIN);
        UserDTO updatedUser = userEndpoint.findOneByLoginUser(newLogin);
        Assert.assertNotNull(updatedUser);
        Assert.assertEquals(newLogin, updatedUser.getLogin());
        userEndpoint.mergeUser(sessionDTO, "user", user, DataType.LOGIN);
    }

}
