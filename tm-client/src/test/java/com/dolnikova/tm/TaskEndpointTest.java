package com.dolnikova.tm;

import com.dolnikova.tm.constant.General;
import com.dolnikova.tm.endpoint.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

public class TaskEndpointTest {

    @Nullable
    private UserEndpoint userEndpoint;
    @Nullable
    private ProjectEndpoint projectEndpoint;
    @Nullable
    private SessionEndpoint sessionEndpoint;
    @Nullable
    private TaskEndpoint taskEndpoint;
    @Nullable
    private SessionDTO sessionDTO;
    @NotNull
    private UserDTO user = new UserDTO();
    @Nullable
    private ProjectDTO project;

    @Before
    public void startTest() throws MalformedURLException {
        System.out.println("[START]");
        URL userUrl = new URL(General.ADDRESS + "UserEndpoint?WSDL");
        QName userQname = new QName("http://endpoint.tm.dolnikova.com/", "UserEndpointService");
        Service userService = Service.create(userUrl, userQname);
        userEndpoint = userService.getPort(UserEndpoint.class);

        URL projectUrl = new URL(General.ADDRESS + "ProjectEndpoint?WSDL");
        QName projectQname = new QName("http://endpoint.tm.dolnikova.com/", "ProjectEndpointService");
        Service projectService = Service.create(projectUrl, projectQname);
        projectEndpoint = projectService.getPort(ProjectEndpoint.class);

        URL sessionUrl = new URL(General.ADDRESS + "SessionEndpoint?WSDL");
        QName sessionQname = new QName("http://endpoint.tm.dolnikova.com/", "SessionEndpointService");
        Service sessionService = Service.create(sessionUrl, sessionQname);
        sessionEndpoint = sessionService.getPort(SessionEndpoint.class);

        URL taskUrl = new URL(General.ADDRESS + "TaskEndpoint?WSDL");
        QName taskQname = new QName("http://endpoint.tm.dolnikova.com/", "TaskEndpointService");
        Service taskService = Service.create(taskUrl, taskQname);
        taskEndpoint = taskService.getPort(TaskEndpoint.class);

        assert userEndpoint != null;
        user = userEndpoint.findOneByLoginUser("user");
        assert sessionEndpoint != null;
        sessionDTO = sessionEndpoint.getSession("user", "user");
        assert projectEndpoint != null;
        project = projectEndpoint.findOneByNameProject(sessionDTO, "test");
    }

    @After
    public void finishTest() {
        System.out.println("[FINISH]");
    }

    @Test
    public void findOneByIdTask() {
        assert taskEndpoint != null;
        TaskDTO taskDTO = taskEndpoint.findOneByIdTask(sessionDTO, "97c59842-9af0-4b3a-88ab-0802ea3cdbec");
        Assert.assertNotNull(taskDTO);
        Assert.assertEquals(taskDTO.getName(), "test");
    }

    @Test
    public void findOneByNameTask() {
        assert taskEndpoint != null;
        TaskDTO taskDTO = taskEndpoint.findOneByNameTask(sessionDTO, "test");
        Assert.assertNotNull(taskDTO);
        Assert.assertEquals(taskDTO.getName(), "test");
    }

    @Test
    public void findAllTask() {
        assert taskEndpoint != null;
        List<TaskDTO> list = taskEndpoint.findAllTask(sessionDTO);
        Assert.assertNotNull(list);
    }

    @Test
    public void findAllByNameTask() {
        assert taskEndpoint != null;
        List<TaskDTO> list = taskEndpoint.findAllByNameTask(sessionDTO, "test");
        Assert.assertNotNull(list);
    }

    @Test
    public void findAllByDescriptionTask() {
        assert taskEndpoint != null;
        List<TaskDTO> list = taskEndpoint.findAllByDescriptionTask(sessionDTO, "test");
        Assert.assertNotNull(list);
    }

    @Test
    public void persistTask() {
        TaskDTO t = new TaskDTO();
        t.setName("persist");
        t.setDescription("persist");
        assert project != null;
        t.setProjectId(project.getId());
        t.setUserId(user.getId());
        assert taskEndpoint != null;
        taskEndpoint.persistTask(sessionDTO, t);
        TaskDTO savedTask = taskEndpoint.findOneByNameTask(sessionDTO, "persist");
        Assert.assertNotNull(savedTask);
        taskEndpoint.removeTask(sessionDTO, savedTask);
    }

    @Test
    public void mergeTask() {
        assert taskEndpoint != null;
        TaskDTO t = taskEndpoint.findOneByNameTask(sessionDTO, "test");
        taskEndpoint.mergeTask(sessionDTO, "persist", t, DataType.NAME);
        TaskDTO updatedTask = taskEndpoint.findOneByNameTask(sessionDTO, "persist");
        Assert.assertNotNull(updatedTask);
        taskEndpoint.mergeTask(sessionDTO, "test", updatedTask, DataType.NAME);
    }

}