package com.dolnikova.tm.entity;

import com.dolnikova.tm.enumerated.Status;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "app_project")
public final class Project extends AbstractEntity implements Serializable {

    private Date dateBegin;

    private Date dateEnd;

    private String name;

    private String description;

    @ManyToOne(targetEntity = User.class)
    @OnDelete(action = OnDeleteAction.CASCADE)
    private User user;

    @Column(updatable = false)
    private Date creationDate = new Date();

    @Enumerated(EnumType.STRING)
    private Status status = Status.PLANNED;

}
