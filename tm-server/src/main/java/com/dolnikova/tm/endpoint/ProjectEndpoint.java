package com.dolnikova.tm.endpoint;

import com.dolnikova.tm.api.ServiceLocator;
import com.dolnikova.tm.dto.ProjectDTO;
import com.dolnikova.tm.dto.SessionDTO;
import com.dolnikova.tm.dto.UserDTO;
import com.dolnikova.tm.entity.Project;
import com.dolnikova.tm.entity.Session;
import com.dolnikova.tm.entity.User;
import com.dolnikova.tm.enumerated.DataType;
import com.dolnikova.tm.enumerated.Role;
import com.dolnikova.tm.util.DtoConversionUtil;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
@WebService
public class ProjectEndpoint extends AbstractEndpoint {

    @WebMethod
    @Nullable
    public ProjectDTO findOneByIdProject(@WebParam(name = "session") @Nullable final SessionDTO sessionDTO,
                                         @WebParam(name = "id") @Nullable String id) {
        if (sessionDTO == null) return null;
        @Nullable final User user = userService.findOneById(sessionDTO.getUserId());
        @Nullable final Session session = DtoConversionUtil.dtoToSession(sessionDTO, user);
        validate(session);
        if (session == null) return null;
        return DtoConversionUtil.projectToDto(projectService.findOneById(session.getUser().getId(), id));
    }

    @WebMethod
    @Nullable
    public ProjectDTO findOneByNameProject(@WebParam(name = "session") @Nullable final SessionDTO sessionDTO,
                                           @WebParam(name = "name") @Nullable String name) {
        if (sessionDTO == null) return null;
        @Nullable final User user = userService.findOneById(sessionDTO.getUserId());
        @Nullable final Session session = DtoConversionUtil.dtoToSession(sessionDTO, user);
        validate(session);
        if (session == null) return null;
        return DtoConversionUtil.projectToDto(projectService.findOneByName(name));
    }

    @WebMethod
    public @Nullable List<ProjectDTO> findAllProject(@WebParam(name = "session") @Nullable final SessionDTO sessionDTO) {
        if (sessionDTO == null) return null;
        @Nullable final User user = userService.findOneById(sessionDTO.getUserId());
        @Nullable final Session session = DtoConversionUtil.dtoToSession(sessionDTO, user);
        validate(session);
        if (session == null) return null;
        @Nullable final List<Project> projects = projectService.findAll(session.getUser().getId());
        if (projects == null) return null;
        @Nullable final List<ProjectDTO> projectsDTOs = new ArrayList<>();
        for (Project project : projects) {
            projectsDTOs.add(DtoConversionUtil.projectToDto(project));
        }
        return projectsDTOs;
    }

    @WebMethod
    public @Nullable List<ProjectDTO> findAllByNameProject(@WebParam(name = "session") @Nullable final SessionDTO sessionDTO,
                                                           @WebParam(name = "text") @Nullable String text) {
        if (sessionDTO == null) return null;
        @Nullable final User user = userService.findOneById(sessionDTO.getUserId());
        @Nullable final Session session = DtoConversionUtil.dtoToSession(sessionDTO, user);
        validate(session);
        if (session == null) return null;
        @Nullable final List<Project> projects = projectService.findAllByName(session.getUser().getId(), text);
        if (projects == null) return null;
        @Nullable final List<ProjectDTO> projectsDTOs = new ArrayList<>();
        for (Project project : projects) {
            projectsDTOs.add(DtoConversionUtil.projectToDto(project));
        }
        return projectsDTOs;
    }

    @WebMethod
    public @Nullable List<ProjectDTO> findAllByDescriptionProject(@WebParam(name = "session") @Nullable final SessionDTO sessionDTO,
                                                                  @WebParam(name = "text") @Nullable String text) {
        if (sessionDTO == null) return null;
        @Nullable final User user = userService.findOneById(sessionDTO.getUserId());
        @Nullable final Session session = DtoConversionUtil.dtoToSession(sessionDTO, user);
        validate(session);
        if (session == null) return null;
        @Nullable final List<Project> projects = projectService.findAllByDescription(session.getUser().getId(), text);
        if (projects == null) return null;
        @Nullable final List<ProjectDTO> projectsDTOs = new ArrayList<>();
        for (Project project : projects) {
            projectsDTOs.add(DtoConversionUtil.projectToDto(project));
        }
        return projectsDTOs;
    }

    @WebMethod
    public void persistProject(@WebParam(name = "session") @Nullable final SessionDTO sessionDTO,
                               @WebParam(name = "entity") @Nullable ProjectDTO projectDTO) {
        if (sessionDTO == null) return;
        @Nullable final User user = userService.findOneById(sessionDTO.getUserId());
        @Nullable final Session session = DtoConversionUtil.dtoToSession(sessionDTO, user);
        validate(session);
        if (session == null || projectDTO == null) return;
        projectService.persist(DtoConversionUtil.dtoToProject(projectDTO, user));
    }

    @WebMethod
    public void persistListProject(@WebParam(name = "session") @Nullable final SessionDTO sessionDTO,
                                   @WebParam(name = "list") @Nullable List<ProjectDTO> list) {
        if (sessionDTO == null) return;
        @Nullable final User user = userService.findOneById(sessionDTO.getUserId());
        @Nullable final Session session = DtoConversionUtil.dtoToSession(sessionDTO, user);
        validate(session);
        if (session == null || list == null) return;
        @Nullable final List<Project> projectList = new ArrayList<>();
        for (ProjectDTO projectDTO : list) {
            @Nullable final User owner = userService.findOneById(projectDTO.getUserId());
            projectList.add(DtoConversionUtil.dtoToProject(projectDTO, owner));
        }
        projectService.persistList(projectList);
    }

    @WebMethod
    public void mergeProject(@WebParam(name = "session") @Nullable final SessionDTO sessionDTO,
                             @WebParam(name = "newData") @Nullable String newData,
                             @WebParam(name = "entityToMerge") @Nullable ProjectDTO projectDTO,
                             @WebParam(name = "dataType") @Nullable DataType dataType) {
        if (sessionDTO == null) return;
        @Nullable final User user = userService.findOneById(sessionDTO.getUserId());
        @Nullable final Session session = DtoConversionUtil.dtoToSession(sessionDTO, user);
        validate(session);
        if (session == null) return;
        @Nullable final Project project = DtoConversionUtil.dtoToProject(projectDTO, user);
        if (project == null) return;
        projectService.merge(newData, project, dataType);
    }

    @WebMethod
    public void removeProject(@WebParam(name = "session") @Nullable final SessionDTO sessionDTO,
                              @WebParam(name = "entity") @Nullable ProjectDTO projectDTO) {
        if (sessionDTO == null) return;
        @Nullable final User user = userService.findOneById(sessionDTO.getUserId());
        @Nullable final Session session = DtoConversionUtil.dtoToSession(sessionDTO, user);
        validate(session);
        if (session == null) return;
        Project project = DtoConversionUtil.dtoToProject(projectDTO, user);
        projectService.remove(project);
    }

    @WebMethod
    public void removeAllProject(@WebParam(name = "session") @Nullable final SessionDTO sessionDTO) {
        if (sessionDTO == null) return;
        @Nullable final User user = userService.findOneById(sessionDTO.getUserId());
        @Nullable final Session session = DtoConversionUtil.dtoToSession(sessionDTO, user);
        validate(session);
        if (session == null) return;
        projectService.removeAll(session.getUser().getId());
    }

    @WebMethod
    public boolean saveBinProject(
            @WebParam(name = "session") @Nullable SessionDTO sessionDTO,
            @WebParam(name = "user") @Nullable UserDTO userDTO,
            @WebParam(name = "projects") @Nullable List<ProjectDTO> entities) throws Exception {
        if (userDTO == null || sessionDTO == null || entities == null) return false;
        if (userDTO.getRole() != Role.ADMIN) return false;
        @Nullable final User user = userService.findOneById(sessionDTO.getUserId());
        @Nullable final Session session = DtoConversionUtil.dtoToSession(sessionDTO, user);
        validate(session);
        @Nullable final List<Project> projectList = new ArrayList<>();
        for (ProjectDTO projectDTO : entities) {
            @Nullable final User owner = userService.findOneById(projectDTO.getUserId());
            projectList.add(DtoConversionUtil.dtoToProject(projectDTO, owner));
        }
        projectService.saveBin(projectList);
        return true;
    }

    @WebMethod
    public boolean saveFasterxmlJsonProject(
            @WebParam(name = "session") @Nullable SessionDTO sessionDTO,
            @WebParam(name = "user") @Nullable UserDTO userDTO,
            @WebParam(name = "projects") @Nullable List<ProjectDTO> entities) throws Exception {
        if (userDTO == null || sessionDTO == null || entities == null) return false;
        if (userDTO.getRole() != Role.ADMIN) return false;
        @Nullable final User user = userService.findOneById(sessionDTO.getUserId());
        @Nullable final Session session = DtoConversionUtil.dtoToSession(sessionDTO, user);
        validate(session);
        @Nullable final List<Project> projectList = new ArrayList<>();
        for (ProjectDTO projectDTO : entities) {
            @Nullable final User owner = userService.findOneById(projectDTO.getUserId());
            projectList.add(DtoConversionUtil.dtoToProject(projectDTO, owner));
        }
        projectService.saveFasterxmlJson(projectList);
        return true;
    }

    @WebMethod
    public boolean saveFasterxmlXmlProject(
            @WebParam(name = "session") @Nullable SessionDTO sessionDTO,
            @WebParam(name = "user") @Nullable UserDTO userDTO,
            @WebParam(name = "projects") @Nullable List<ProjectDTO> entities) throws Exception {
        if (userDTO == null || sessionDTO == null || entities == null) return false;
        if (userDTO.getRole() != Role.ADMIN) return false;
        @Nullable final User user = userService.findOneById(sessionDTO.getUserId());
        @Nullable final Session session = DtoConversionUtil.dtoToSession(sessionDTO, user);
        validate(session);
        @Nullable final List<Project> projectList = new ArrayList<>();
        for (ProjectDTO projectDTO : entities) {
            @Nullable final User owner = userService.findOneById(projectDTO.getUserId());
            projectList.add(DtoConversionUtil.dtoToProject(projectDTO, owner));
        }
        projectService.saveFasterxmlXml(projectList);
        return true;
    }

    @WebMethod
    public boolean saveJaxbJsonProject(
            @WebParam(name = "session") @Nullable SessionDTO sessionDTO,
            @WebParam(name = "user") @Nullable UserDTO userDTO,
            @WebParam(name = "projects") @Nullable List<ProjectDTO> entities) throws Exception {
        if (userDTO == null || sessionDTO == null || entities == null) return false;
        if (userDTO.getRole() != Role.ADMIN) return false;
        @Nullable final User user = userService.findOneById(sessionDTO.getUserId());
        @Nullable final Session session = DtoConversionUtil.dtoToSession(sessionDTO, user);
        validate(session);
        @Nullable final List<Project> projectList = new ArrayList<>();
        for (ProjectDTO projectDTO : entities) {
            @Nullable final User owner = userService.findOneById(projectDTO.getUserId());
            projectList.add(DtoConversionUtil.dtoToProject(projectDTO, owner));
        }
        projectService.saveJaxbJson(projectList);
        return true;
    }

    @WebMethod
    public boolean saveJaxbXmlProject(
            @WebParam(name = "session") @Nullable SessionDTO sessionDTO,
            @WebParam(name = "user") @Nullable UserDTO userDTO,
            @WebParam(name = "projects") @Nullable List<ProjectDTO> entities) throws Exception {
        if (userDTO == null || sessionDTO == null || entities == null) return false;
        if (userDTO.getRole() != Role.ADMIN) return false;
        @Nullable final User user = userService.findOneById(sessionDTO.getUserId());
        @Nullable final Session session = DtoConversionUtil.dtoToSession(sessionDTO, user);
        validate(session);
        @Nullable final List<Project> projectList = new ArrayList<>();
        for (ProjectDTO projectDTO : entities) {
            @Nullable final User owner = userService.findOneById(projectDTO.getUserId());
            projectList.add(DtoConversionUtil.dtoToProject(projectDTO, owner));
        }
        projectService.saveJaxbXml(projectList);
        return true;
    }

    @WebMethod
    public @Nullable List<ProjectDTO> loadBinProject(
            @WebParam(name = "session") @Nullable SessionDTO sessionDTO,
            @WebParam(name = "user") @Nullable UserDTO userDTO) throws Exception {
        if (sessionDTO == null) return null;
        @Nullable final User user = userService.findOneById(sessionDTO.getUserId());
        @Nullable final Session session = DtoConversionUtil.dtoToSession(sessionDTO, user);
        validate(session);
        if (userDTO == null) return null;
        if (userDTO.getRole() != Role.ADMIN) throw new IllegalAccessException();
        @Nullable final List<Project> projects = projectService.loadBin();
        if (projects == null) return null;
        @Nullable final List<ProjectDTO> projectsDTOs = new ArrayList<>();
        for (Project project : projects) {
            projectsDTOs.add(DtoConversionUtil.projectToDto(project));
        }
        return projectsDTOs;
    }

    @WebMethod
    public @Nullable List<ProjectDTO> loadFasterxmlJsonProject(
            @WebParam(name = "session") @Nullable SessionDTO sessionDTO,
            @WebParam(name = "user") @Nullable UserDTO userDTO) throws Exception {
        if (sessionDTO == null) return null;
        @Nullable final User user = userService.findOneById(sessionDTO.getUserId());
        @Nullable final Session session = DtoConversionUtil.dtoToSession(sessionDTO, user);
        validate(session);
        if (userDTO == null) return null;
        if (userDTO.getRole() != Role.ADMIN) throw new IllegalAccessException();
        @Nullable final List<Project> projects = projectService.loadFasterxmlJson();
        if (projects == null) return null;
        @Nullable final List<ProjectDTO> projectsDTOs = new ArrayList<>();
        for (Project project : projects) {
            projectsDTOs.add(DtoConversionUtil.projectToDto(project));
        }
        return projectsDTOs;
    }

    @WebMethod
    public @Nullable List<ProjectDTO> loadFasterxmlXmlProject(
            @WebParam(name = "session") @Nullable SessionDTO sessionDTO,
            @WebParam(name = "user") @Nullable UserDTO userDTO) throws Exception {
        if (sessionDTO == null) return null;
        @Nullable final User user = userService.findOneById(sessionDTO.getUserId());
        @Nullable final Session session = DtoConversionUtil.dtoToSession(sessionDTO, user);
        validate(session);
        if (userDTO == null) return null;
        if (userDTO.getRole() != Role.ADMIN) throw new IllegalAccessException();
        @Nullable final List<Project> projects = projectService.loadFasterxmlXml();
        if (projects == null) return null;
        @Nullable final List<ProjectDTO> projectsDTOs = new ArrayList<>();
        for (Project project : projects) {
            projectsDTOs.add(DtoConversionUtil.projectToDto(project));
        }
        return projectsDTOs;
    }

    @WebMethod
    public @Nullable List<ProjectDTO> loadJaxbJsonProject(
            @WebParam(name = "session") @Nullable SessionDTO sessionDTO,
            @WebParam(name = "user") @Nullable UserDTO userDTO) throws Exception {
        if (sessionDTO == null) return null;
        @Nullable final User user = userService.findOneById(sessionDTO.getUserId());
        @Nullable final Session session = DtoConversionUtil.dtoToSession(sessionDTO, user);
        validate(session);
        if (userDTO == null) return null;
        if (userDTO.getRole() != Role.ADMIN) throw new IllegalAccessException();
        @Nullable final List<Project> projects = projectService.loadJaxbJson();
        if (projects == null) return null;
        @Nullable final List<ProjectDTO> projectsDTOs = new ArrayList<>();
        for (Project project : projects) {
            projectsDTOs.add(DtoConversionUtil.projectToDto(project));
        }
        return projectsDTOs;
    }

    @WebMethod
    public @Nullable List<ProjectDTO> loadJaxbXmlProject(
            @WebParam(name = "session") @Nullable SessionDTO sessionDTO,
            @WebParam(name = "user") @Nullable UserDTO userDTO) throws Exception {
        if (sessionDTO == null) return null;
        @Nullable final User user = userService.findOneById(sessionDTO.getUserId());
        @Nullable final Session session = DtoConversionUtil.dtoToSession(sessionDTO, user);
        validate(session);
        if (userDTO == null) return null;
        if (userDTO.getRole() != Role.ADMIN) throw new IllegalAccessException();
        @Nullable final List<Project> projects = projectService.loadJaxbXml();
        if (projects == null) return null;
        @Nullable final List<ProjectDTO> projectsDTOs = new ArrayList<>();
        for (Project project : projects) {
            projectsDTOs.add(DtoConversionUtil.projectToDto(project));
        }
        return projectsDTOs;
    }

}
