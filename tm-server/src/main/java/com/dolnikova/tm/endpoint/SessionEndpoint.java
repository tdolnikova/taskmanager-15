package com.dolnikova.tm.endpoint;

import com.dolnikova.tm.dto.SessionDTO;
import com.dolnikova.tm.dto.UserDTO;
import com.dolnikova.tm.entity.Session;
import com.dolnikova.tm.entity.User;
import com.dolnikova.tm.enumerated.Role;
import com.dolnikova.tm.util.DtoConversionUtil;
import com.dolnikova.tm.util.PasswordHashUtil;
import com.dolnikova.tm.util.SignatureUtil;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
@WebService
public class SessionEndpoint extends AbstractEndpoint {

    @WebMethod
    public SessionDTO getSession(@WebParam(name = "login") @Nullable final String login,
                                 @WebParam(name = "password") @Nullable final String password) {
        if (login == null || login.isEmpty()) return null;
        if (password == null || password.isEmpty()) return null;
        User user = userService.findOneByLogin(login);
        System.out.println("Запрос сессии пользователя: " + user.getId() + " " + user.getLogin());
        String hashedPassword = PasswordHashUtil.md5(password);
        if (!hashedPassword.equals(user.getPasswordHash())) {
            System.out.println("User not found");
            return null;
        }
        @NotNull final Session session = new Session();
        session.setUser(user);
        @Nullable final String signature = SignatureUtil.sign(password, "JAVA", 3);
        session.setSignature(signature);
        sessionService.persist(session);
        return DtoConversionUtil.sessionToDto(session);
    }

    @WebMethod
    public SessionDTO findOneByUserIdSession(
            @WebParam(name = "userId") @Nullable String userId) {
        if (userId == null || userId.isEmpty()) return null;
        return DtoConversionUtil.sessionToDto(sessionService.findOneByUserId(userId));
    }

    public SessionDTO findOneByIdSession(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) return null;
        if (id == null || id.isEmpty()) return null;
        return DtoConversionUtil.sessionToDto(sessionService.findOneById(userId, id));
    }

    @Nullable
    public SessionDTO findOneBySignature(@Nullable final String signature) {
        if (signature == null || signature.isEmpty()) return null;
        return DtoConversionUtil.sessionToDto(sessionService.findOneBySignature(signature));
    }


    public @Nullable List<SessionDTO> findAllSession(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) return null;
        List<Session> sessionList = sessionService.findAll(userId);
        if (sessionList == null || sessionList.isEmpty()) return null;
        List<SessionDTO> dtoList = new ArrayList<>();
        for (Session session : sessionList) {
            dtoList.add(DtoConversionUtil.sessionToDto(session));
        }
        return dtoList;
    }

    public void persistSession(@Nullable final SessionDTO session, @Nullable final UserDTO userDTO) {
        if (session == null || userDTO == null) return;
        @NotNull final User user = DtoConversionUtil.dtoToUser(userDTO);
        sessionService.persist(DtoConversionUtil.dtoToSession(session, user));
    }


    public void removeSession(@Nullable final SessionDTO session, @Nullable final UserDTO userDTO) {
        if (session == null || userDTO == null) return;
        @NotNull final User user = DtoConversionUtil.dtoToUser(userDTO);
        sessionService.remove(DtoConversionUtil.dtoToSession(session, user));
    }

    @WebMethod
    public boolean saveBinSession(
            @WebParam(name = "session") @Nullable SessionDTO sessionDTO,
            @WebParam(name = "user") @Nullable UserDTO userDTO,
            @WebParam(name = "sessions") @Nullable List<SessionDTO> entities) throws Exception {
        if (userDTO == null || sessionDTO == null || entities == null) return false;
        if (userDTO.getRole() != Role.ADMIN) return false;
        @Nullable final User user = userService.findOneById(sessionDTO.getUserId());
        @Nullable final Session session = DtoConversionUtil.dtoToSession(sessionDTO, user);
        validate(session);
        sessionService.saveBin(dtoToSessionList(entities));
        return true;
    }

    @WebMethod
    public boolean saveFasterxmlJsonSession(
            @WebParam(name = "session") @Nullable SessionDTO sessionDTO,
            @WebParam(name = "user") @Nullable UserDTO userDTO,
            @WebParam(name = "sessions") @Nullable List<SessionDTO> entities) throws Exception {
        if (userDTO == null || sessionDTO == null || entities == null) return false;
        if (userDTO.getRole() != Role.ADMIN) return false;
        @Nullable final User user = userService.findOneById(sessionDTO.getUserId());
        @Nullable final Session session = DtoConversionUtil.dtoToSession(sessionDTO, user);
        validate(session);
        sessionService.saveFasterxmlJson(dtoToSessionList(entities));
        return true;
    }

    @WebMethod
    public boolean saveFasterxmlXmlSession(
            @WebParam(name = "session") @Nullable SessionDTO sessionDTO,
            @WebParam(name = "user") @Nullable UserDTO userDTO,
            @WebParam(name = "sessions") @Nullable List<SessionDTO> entities) throws Exception {
        if (userDTO == null || sessionDTO == null || entities == null) return false;
        if (userDTO.getRole() != Role.ADMIN) return false;
        @Nullable final User user = userService.findOneById(sessionDTO.getUserId());
        @Nullable final Session session = DtoConversionUtil.dtoToSession(sessionDTO, user);
        validate(session);
        sessionService.saveFasterxmlXml(dtoToSessionList(entities));
        return true;
    }

    @WebMethod
    public boolean saveJaxbJsonSession(
            @WebParam(name = "session") @Nullable SessionDTO sessionDTO,
            @WebParam(name = "user") @Nullable UserDTO userDTO,
            @WebParam(name = "sessions") @Nullable List<SessionDTO> entities) throws Exception {
        if (userDTO == null || sessionDTO == null || entities == null) return false;
        if (userDTO.getRole() != Role.ADMIN) return false;
        @Nullable final User user = userService.findOneById(sessionDTO.getUserId());
        @Nullable final Session session = DtoConversionUtil.dtoToSession(sessionDTO, user);
        validate(session);
        sessionService.saveJaxbJson(dtoToSessionList(entities));
        return true;
    }

    @WebMethod
    public boolean saveJaxbXmlSession(
            @WebParam(name = "session") @Nullable SessionDTO sessionDTO,
            @WebParam(name = "user") @Nullable UserDTO userDTO,
            @WebParam(name = "sessions") @Nullable List<SessionDTO> entities) throws Exception {
        if (userDTO == null || sessionDTO == null || entities == null) return false;
        if (userDTO.getRole() != Role.ADMIN) return false;
        @Nullable final User user = userService.findOneById(sessionDTO.getUserId());
        @Nullable final Session session = DtoConversionUtil.dtoToSession(sessionDTO, user);
        validate(session);
        sessionService.saveJaxbXml(dtoToSessionList(entities));
        return true;
    }

    @WebMethod
    public @Nullable List<SessionDTO> loadBinSession(
            @WebParam(name = "session") @Nullable SessionDTO sessionDTO,
            @WebParam(name = "user") @Nullable UserDTO userDTO) throws Exception {
        if (userDTO == null || sessionDTO == null) return null;
        if (userDTO.getRole() != Role.ADMIN) return null;
        @Nullable final User user = userService.findOneById(sessionDTO.getUserId());
        @Nullable final Session session = DtoConversionUtil.dtoToSession(sessionDTO, user);
        validate(session);
        return sessionToDtoList(sessionService.loadBin());
    }

    @WebMethod
    public @Nullable List<SessionDTO> loadFasterxmlJsonSession(
            @WebParam(name = "session") @Nullable SessionDTO sessionDTO,
            @WebParam(name = "user") @Nullable UserDTO userDTO) throws Exception {
        if (userDTO == null || sessionDTO == null) return null;
        if (userDTO.getRole() != Role.ADMIN) return null;
        @Nullable final User user = userService.findOneById(sessionDTO.getUserId());
        @Nullable final Session session = DtoConversionUtil.dtoToSession(sessionDTO, user);
        validate(session);
        return sessionToDtoList(sessionService.loadBin());
    }

    @WebMethod
    public @Nullable List<SessionDTO> loadFasterxmlXmlSession(
            @WebParam(name = "session") @Nullable SessionDTO sessionDTO,
            @WebParam(name = "user") @Nullable UserDTO userDTO) throws Exception {
        if (userDTO == null || sessionDTO == null) return null;
        if (userDTO.getRole() != Role.ADMIN) return null;
        @Nullable final User user = userService.findOneById(sessionDTO.getUserId());
        @Nullable final Session session = DtoConversionUtil.dtoToSession(sessionDTO, user);
        validate(session);
        return sessionToDtoList(sessionService.loadBin());
    }

    @WebMethod
    public @Nullable List<SessionDTO> loadJaxbJsonSession(
            @WebParam(name = "session") @Nullable SessionDTO sessionDTO,
            @WebParam(name = "user") @Nullable UserDTO userDTO) throws Exception {
        if (userDTO == null || sessionDTO == null) return null;
        if (userDTO.getRole() != Role.ADMIN) return null;
        @Nullable final User user = userService.findOneById(sessionDTO.getUserId());
        @Nullable final Session session = DtoConversionUtil.dtoToSession(sessionDTO, user);
        validate(session);
        return sessionToDtoList(sessionService.loadBin());
    }

    @WebMethod
    public @Nullable List<SessionDTO> loadJaxbXmlSession(
            @WebParam(name = "session") @Nullable SessionDTO sessionDTO,
            @WebParam(name = "user ") @Nullable UserDTO userDTO) throws Exception {
        if (userDTO == null || sessionDTO == null) return null;
        if (userDTO.getRole() != Role.ADMIN) return null;
        @Nullable final User user = userService.findOneById(sessionDTO.getUserId());
        @Nullable final Session session = DtoConversionUtil.dtoToSession(sessionDTO, user);
        validate(session);
        return sessionToDtoList(sessionService.loadBin());
    }

    @Nullable
    private List<SessionDTO> sessionToDtoList(@Nullable final List<Session> sessions) {
        if (sessions == null) return null;
        @Nullable final List<SessionDTO> sessionDTOS = new ArrayList<>();
        for (Session session : sessions) {
            sessionDTOS.add(DtoConversionUtil.sessionToDto(session));
        }
        return sessionDTOS;
    }

    @Nullable
    private List<Session> dtoToSessionList(@Nullable final List<SessionDTO> sessionDTOS) {
        if (sessionDTOS == null) return null;
        @Nullable final List<Session> sessions = new ArrayList<>();
        for (SessionDTO sessionDTO : sessionDTOS) {
            @Nullable final User user = userService.findOneById(sessionDTO.getUserId());
            sessions.add(DtoConversionUtil.dtoToSession(sessionDTO, user));
        }
        return sessions;
    }

}
