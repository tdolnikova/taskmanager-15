package com.dolnikova.tm.mapper;

import com.dolnikova.tm.entity.Session;
import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface SessionMapper {

    @Nullable
    @Select("SELECT * FROM app_session WHERE user_id = #{userId}")
    @ResultMap("sessionResultMap")
    Session findOneByUserId(@NotNull @Param("userId") String userId);

    @Nullable
    @Select("SELECT * FROM app_session WHERE id = #{id}")
    @ResultMap("sessionResultMap")
    Session findOneById(@NotNull @Param("id") String id);

    @Nullable
    @Select("SELECT * FROM app_session WHERE signature = #{signature}")
    @ResultMap("sessionResultMap")
    Session findOneBySignature(@NotNull @Param("signature") String signature);

    @Nullable
    @Select("SELECT * FROM app_session WHERE user_id = #{userId}")
    @ResultMap("sessionResultMap")
    List<Session> findAll(@NotNull @Param("userId") String userId);

    @Insert("INSERT INTO app_session (id, signature, timestamp, user_id) values (#{id}, #{signature}, #{timestamp}, #{userId})")
    void persist(@NotNull @Param("id") String id, @NotNull @Param("signature") String signature, @NotNull @Param("timestamp") Long timestamp, @NotNull @Param("userId") String userId);

    @Delete("DELETE FROM app_session WHERE id = #{id}")
    void remove(@NotNull @Param("id") String id);

    @Delete("DELETE FROM app_session WHERE user_id = #{user_id}")
    void removeAll(@NotNull @Param("userId") String userId);

}
