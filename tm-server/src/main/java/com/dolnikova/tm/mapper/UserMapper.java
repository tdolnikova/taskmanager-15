package com.dolnikova.tm.mapper;

import com.dolnikova.tm.entity.User;
import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface UserMapper {

    @Nullable
    @Select("SELECT * FROM app_user WHERE id = #{id}")
    @ResultMap("userResultMap")
    User findOneByUserId(@NotNull @Param("id") String id);

    @Nullable
    @Select("SELECT * FROM app_user WHERE login = #{login}")
    @ResultMap("userResultMap")
    User findOneByLogin(@NotNull String login);

    @Nullable
    @Select("SELECT * FROM app_user")
    @ResultMap("userResultMap")
    List<User> findAll();

    @Nullable
    @Select("SELECT * FROM app_user WHERE login LIKE #{login}")
    @ResultMap("userResultMap")
    List<User> findAllByLogin(@NotNull @Param("login") String login);

    @Insert("INSERT INTO app_user (id, login, passwordHash) values (#{id}, #{login}, #{passwordHash})")
    void persist(@NotNull @Param("id") String id, @NotNull @Param("login") String login, @NotNull @Param("passwordHash") String passwordHash);

    @Update("UPDATE app_user SET passwordHash = #{passwordHash} WHERE id = #{id}")
    void updatePassword(@NotNull @Param("passwordHash") String passwordHash, @NotNull @Param("id") String id);

    @Update("UPDATE app_user SET login = #{login} WHERE id = #{id}")
    void updateaLogin(@NotNull @Param("login") String login, @NotNull @Param("id") String id);

    @Update("UPDATE app_user SET role = #{role} WHERE id = #{id}")
    void updateRole(@NotNull @Param("role") String role, @NotNull @Param("id") String id);

    @Delete("DELETE FROM app_user WHERE id = #{id}")
    void remove(@NotNull @Param("id") String id);

    @Delete("DELETE FROM app_user")
    void removeAll();

}
