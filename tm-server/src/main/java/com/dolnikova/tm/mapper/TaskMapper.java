package com.dolnikova.tm.mapper;

import com.dolnikova.tm.entity.Task;
import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface TaskMapper {

    @Nullable
    @Select("SELECT * FROM app_task WHERE id = #{id}")
    @ResultMap("taskResultMap")
    Task findOneByUserId(@NotNull @Param("id") String id);

    @Nullable
    @Select("SELECT * FROM app_task WHERE id = #{id} AND user_id = #{userId}")
    @ResultMap("taskResultMap")
    Task findOneByUserIdAndId(@NotNull @Param("userId") String userId, @NotNull @Param("id") String id);

    @Nullable
    @Select("SELECT * FROM app_task WHERE name = #{name}")
    @ResultMap("taskResultMap")
    Task findOneByName(@NotNull @Param("name") String name);

    @Nullable
    @Select("SELECT * FROM app_task WHERE user_id = #{userId}")
    @ResultMap("taskResultMap")
    List<Task> findAll(@NotNull @Param("userId") String userId);

    @Nullable
    @Select("SELECT * FROM app_task WHERE user_id = #{userId} AND name LIKE #{name}")
    @ResultMap("taskResultMap")
    List<Task> findAllByName(@NotNull @Param("userId") String userId, @NotNull @Param("name") String name);

    @Nullable
    @Select("SELECT * FROM app_task WHERE user_id = #{userId} AND description LIKE #{description}")
    @ResultMap("taskResultMap")
    List<Task> findAllByDescription(@NotNull @Param("userId") String userId, @NotNull @Param("description") String description);

    @Insert("INSERT INTO app_task (id, name, project_id, user_id) values (#{id}, #{name}, #{projectId}, #{userId})")
    void persist(@NotNull @Param("id") String id, @NotNull @Param("name") String name, @NotNull @Param("projectId") String projectId, @NotNull @Param("userId") String userId);

    @Update("UPDATE app_task SET name = #{name} WHERE id = #{id}")
    void updateName(@NotNull @Param("name") String name, @NotNull @Param("id") String id);

    @Update("UPDATE app_task SET description = #{description} WHERE id = #{id}")
    void updateDescription(@NotNull @Param("description") String description, @NotNull @Param("id") String id);

    @Delete("DELETE FROM app_task WHERE id = #{id}")
    void remove(@NotNull @Param("id") String id);

    @Delete("DELETE FROM app_task WHERE user_id = #{userId}")
    void removeAll(@NotNull @Param("userId") String userId);

}
