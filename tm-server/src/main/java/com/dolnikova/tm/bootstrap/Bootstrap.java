package com.dolnikova.tm.bootstrap;


import com.dolnikova.tm.constant.General;
import com.dolnikova.tm.endpoint.ProjectEndpoint;
import com.dolnikova.tm.endpoint.SessionEndpoint;
import com.dolnikova.tm.endpoint.TaskEndpoint;
import com.dolnikova.tm.endpoint.UserEndpoint;
import com.dolnikova.tm.util.DatabaseUtil;
import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.xml.ws.Endpoint;
import java.sql.Connection;
import java.util.Scanner;

@Getter
@Setter
@Singleton
public final class Bootstrap {

    @Inject
    private UserEndpoint userEndpoint;
    @Inject
    private TaskEndpoint taskEndpoint;
    @Inject
    private ProjectEndpoint projectEndpoint;
    @Inject
    private SessionEndpoint sessionEndpoint;
    @Nullable
    public static Connection connection;

    public void init() {
        connection = DatabaseUtil.getConnection();
        start();
    }

    private void start() {
        Endpoint.publish(General.ADDRESS + "TaskEndpoint?WSDL", taskEndpoint);
        Endpoint.publish(General.ADDRESS + "ProjectEndpoint?WSDL", projectEndpoint);
        Endpoint.publish(General.ADDRESS + "UserEndpoint?WSDL", userEndpoint);
        Endpoint.publish(General.ADDRESS + "SessionEndpoint?WSDL", sessionEndpoint);

        System.out.println(General.ADDRESS + "TaskEndpoint?WSDL");
        System.out.println(General.ADDRESS + "ProjectEndpoint?WSDL");
        System.out.println(General.ADDRESS + "UserEndpoint?WSDL");
        System.out.println(General.ADDRESS + "SessionEndpoint?WSDL");

        Scanner scanner = new Scanner(System.in);
        String input = "";
        while (!"exit".equals(input)) {
            try {
                input = scanner.nextLine();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        try {
            if (connection != null) connection.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.exit(0);
    }

}
