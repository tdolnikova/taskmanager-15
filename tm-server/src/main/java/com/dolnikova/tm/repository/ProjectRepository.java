package com.dolnikova.tm.repository;

import com.dolnikova.tm.api.repository.IProjectRepository;
import com.dolnikova.tm.constant.General;
import com.dolnikova.tm.entity.Project;
import com.dolnikova.tm.enumerated.DataType;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.persistence.EntityManager;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.*;
import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
@Setter
public final class ProjectRepository extends AbstractRepository<Project> implements IProjectRepository {

    private EntityManager entityManager;

    public ProjectRepository(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Nullable
    @Override
    public Project findOneById(@NotNull final String userId, @NotNull final String id) {
        @Nullable final Project project = entityManager.find(Project.class, id);
        System.out.println("Project found:\nid: " + id + " user_id: " + userId);
        if (project == null) return null;
        System.out.println("user_id in project entity: " + project.getUser().getId());
        if (userId.equals(project.getUser().getId())) return project;
        return null;
    }

    @Override
    public Project findOneByName(@NotNull final String name) {
        return entityManager.
                createQuery("SELECT p from Project p where p.name = :name", Project.class).
                setParameter("name", name).
                getSingleResult();
    }

    @Override
    public @Nullable List<Project> findAll(@NotNull final String userId) {
        @Nullable final List<Project> projects = entityManager.
                createQuery("SELECT p FROM Project p WHERE p.user.id = :userId", Project.class)
                .setParameter("userId", userId)
                .getResultList();
        return projects;
    }

    @Override
    public @Nullable List<Project> findAllByName(@NotNull final String ownerId, @NotNull final String text) {
        @Nullable final List<Project> projects = entityManager.
                createQuery("SELECT p FROM Project p WHERE p.name LIKE :text", Project.class).
                setParameter("text", text).
                getResultList();
        return projects;
    }

    @Override
    public @Nullable List<Project> findAllByDescription(@NotNull final String ownerId, @NotNull final String text) {
        @Nullable final List<Project> projects = entityManager.
                createQuery("SELECT p FROM Project p WHERE p.description LIKE :text", Project.class).
                setParameter("text", text).
                getResultList();
        return projects;
    }

    @Override
    public void persist(@NotNull final Project project) {
        entityManager.persist(project);
    }

    @Override
    public void merge(@NotNull final String newData,
                      @NotNull final Project dbProject,
                      @NotNull final DataType dataType) {
        if (dataType.equals(DataType.NAME)) {
            dbProject.setName(newData);
            entityManager.merge(dbProject);
        }
        if (dataType.equals(DataType.DESCRIPTION)) {
            dbProject.setDescription(newData);
            entityManager.merge(dbProject);
        }
    }

    @Override
    public void remove(@NotNull final Project dbProject) {
        entityManager.remove(entityManager.find(Project.class, dbProject.getId()));
    }

    @Override
    public void saveBin(@NotNull final List<Project> entities) throws Exception {
        @Nullable final File path = new File(General.SAVE_PATH + General.PROJECTS_BIN);
        if (!path.exists()) {
            path.getParentFile().mkdirs();
            path.createNewFile();
        }
        try (@Nullable final ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream(path))) {
            objectOutputStream.writeObject(new ArrayList<>(entities));
        }
    }

    @Override
    public void saveFasterxmlJson(@NotNull final List<Project> entities) throws Exception {
        @Nullable final File path = new File(General.SAVE_PATH + General.PROJECTS_JSON);
        if (!path.exists()) {
            path.getParentFile().mkdirs();
            path.createNewFile();
        }
        @Nullable final ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.writerWithDefaultPrettyPrinter().writeValue(path, new ArrayList<>(entities));
    }

    @Override
    public void saveFasterxmlXml(@NotNull final List<Project> entities) throws Exception {
        @Nullable final File path = new File(General.SAVE_PATH + General.PROJECTS_XML);
        if (!path.exists()) {
            path.getParentFile().mkdirs();
            path.createNewFile();
        }
        @Nullable final XmlMapper xmlMapper = new XmlMapper();
        xmlMapper.writerWithDefaultPrettyPrinter().writeValue(path, new ArrayList<>(entities));
        System.out.println(this.getClass().getName());
    }

    @Override
    public void saveJaxbJson(@NotNull final List<Project> entities) throws Exception {
        @Nullable final File path = new File(General.SAVE_PATH + General.PROJECTS_JSON);
        if (!path.exists()) {
            path.getParentFile().mkdirs();
            path.createNewFile();
        }
        @NotNull final JAXBContext jaxbContext = JAXBContext.newInstance(ArrayList.class);
        @NotNull final Marshaller marshaller = jaxbContext.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        marshaller.marshal(new ArrayList<>(entities), path);
    }

    @Override
    public void saveJaxbXml(@NotNull final List<Project> entities) throws Exception {
        @Nullable final File path = new File(General.SAVE_PATH + General.PROJECTS_XML);
        if (!path.exists()) {
            path.getParentFile().mkdirs();
            path.createNewFile();
        }
        @NotNull final JAXBContext jaxbContext = JAXBContext.newInstance(ArrayList.class);
        @NotNull final Marshaller marshaller = jaxbContext.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        marshaller.marshal(new ArrayList<>(entities), path);
    }

    @Nullable
    @Override
    public List<Project> loadBin() throws Exception {
        @Nullable final File file = new File(General.SAVE_PATH + General.PROJECTS_BIN);
        try (@Nullable final ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream(file))) {
            @Nullable final ArrayList<Project> projects = (ArrayList) objectInputStream.readObject();
            return projects;
        }
    }

    @Nullable
    @Override
    public List<Project> loadFasterxmlJson() throws Exception {
        @Nullable final File file = new File(General.SAVE_PATH + General.PROJECTS_JSON);
        @Nullable final ObjectMapper objectMapper = new ObjectMapper();
        @Nullable final ArrayList<Project> projects = objectMapper.readValue(file, ArrayList.class);
        return projects;
    }

    @Nullable
    @Override
    public List<Project> loadFasterxmlXml() throws Exception {
        @Nullable final File file = new File(General.SAVE_PATH + General.PROJECTS_XML);
        @Nullable final XmlMapper xmlMapper = new XmlMapper();
        @Nullable final ArrayList<Project> projects = xmlMapper.readValue(file, ArrayList.class);
        return projects;
    }

    @Nullable
    @Override
    public List<Project> loadJaxbJson() throws Exception {
        @Nullable final File file = new File(General.SAVE_PATH + General.PROJECTS_JSON);
        @NotNull final JAXBContext jaxbContext = JAXBContext.newInstance(ArrayList.class);
        @Nullable final Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
        @Nullable final ArrayList<Project> projects = (ArrayList<Project>) unmarshaller.unmarshal(file);
        return projects;
    }

    @Nullable
    @Override
    public List<Project> loadJaxbXml() throws Exception {
        @Nullable final File file = new File(General.SAVE_PATH + General.PROJECTS_XML);
        @NotNull final JAXBContext jaxbContext = JAXBContext.newInstance(ArrayList.class);
        @Nullable final Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
        @Nullable final ArrayList<Project> projects = (ArrayList<Project>) unmarshaller.unmarshal(file);
        return projects;
    }

}
