package com.dolnikova.tm.repository;

import com.dolnikova.tm.api.repository.ITaskRepository;
import com.dolnikova.tm.constant.General;
import com.dolnikova.tm.entity.Task;
import com.dolnikova.tm.enumerated.DataType;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.persistence.EntityManager;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.*;
import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
@Setter
public final class TaskRepository extends AbstractRepository<Task> implements ITaskRepository {

    private EntityManager entityManager;

    public TaskRepository(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Nullable
    @Override
    public Task findOneById(@NotNull final String userId, @NotNull final String id) {
        @Nullable final Task task = entityManager.find(Task.class, id);
        if (task == null) return null;
        if (userId.equals(task.getUser().getId())) return task;
        return null;
    }

    @Override
    public Task findOneByName(@NotNull final String name) {
        return entityManager.
                createQuery("SELECT t from Task t where t.name = :name", Task.class).
                setParameter("name", name).
                getSingleResult();
    }

    @Override
    public @Nullable List<Task> findAll(@NotNull final String userId) {
        @Nullable final List<Task> tasks = entityManager.
                createQuery("SELECT t FROM Task t WHERE t.user.id = :userId", Task.class)
                .setParameter("userId", userId)
                .getResultList();
        return tasks;
    }

    @Override
    public @Nullable List<Task> findAllByName(@NotNull final String ownerId, @NotNull final String text) {
        @Nullable final List<Task> tasks = entityManager.
                createQuery("SELECT t FROM Task t WHERE t.name LIKE :text", Task.class).
                setParameter("text", text).
                getResultList();
        return tasks;
    }

    @Override
    public @Nullable List<Task> findAllByDescription(@NotNull final String ownerId, @NotNull final String text) {
        @Nullable final List<Task> tasks = entityManager.
                createQuery("SELECT t FROM Task t WHERE t.description LIKE :text", Task.class).
                setParameter("text", text).
                getResultList();
        return tasks;
    }

    @Override
    public void persist(@NotNull final Task task) {
        entityManager.persist(task);
    }

    @Override
    public void merge(@NotNull final String newData,
                      @NotNull final Task dbTask,
                      @NotNull final DataType dataType) {
        if (dataType.equals(DataType.NAME)) {
            dbTask.setName(newData);
        }
        if (dataType.equals(DataType.DESCRIPTION)) {
            dbTask.setDescription(newData);
        }
    }

    @Override
    public void remove(@NotNull final Task dbTask) {
        entityManager.remove(dbTask);
    }

    @Override
    public void saveBin(@NotNull final List<Task> entities) throws Exception {
        @Nullable final File path = new File(General.SAVE_PATH + General.TASKS_BIN);
        if (!path.exists()) {
            path.getParentFile().mkdirs();
            path.createNewFile();
        }
        try (@Nullable final ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream(path))) {
            objectOutputStream.writeObject(new ArrayList<>(entities));
        }
    }

    @Override
    public void saveFasterxmlJson(@NotNull final List<Task> entities) throws Exception {
        @Nullable final File path = new File(General.SAVE_PATH + General.TASKS_JSON);
        if (!path.exists()) {
            path.getParentFile().mkdirs();
            path.createNewFile();
        }
        @Nullable final ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.writerWithDefaultPrettyPrinter().writeValue(path, new ArrayList<>(entities));
    }

    @Override
    public void saveFasterxmlXml(@NotNull final List<Task> entities) throws Exception {
        @Nullable final File path = new File(General.SAVE_PATH + General.TASKS_XML);
        if (!path.exists()) {
            path.getParentFile().mkdirs();
            path.createNewFile();
        }
        @Nullable final XmlMapper xmlMapper = new XmlMapper();
        xmlMapper.writerWithDefaultPrettyPrinter().writeValue(path, new ArrayList<>(entities));
    }

    @Override
    public void saveJaxbJson(@NotNull final List<Task> entities) throws Exception {
        @Nullable final File path = new File(General.SAVE_PATH + General.TASKS_JSON);
        if (!path.exists()) {
            path.getParentFile().mkdirs();
            path.createNewFile();
        }
        @Nullable final JAXBContext jaxbContext = JAXBContext.newInstance(ArrayList.class);
        @Nullable final Marshaller marshaller = jaxbContext.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        marshaller.marshal(new ArrayList<>(entities), path);
    }

    @Override
    public void saveJaxbXml(@NotNull final List<Task> entities) throws Exception {
        @Nullable final File path = new File(General.SAVE_PATH + General.TASKS_XML);
        if (!path.exists()) {
            path.getParentFile().mkdirs();
            path.createNewFile();
        }
        @Nullable final JAXBContext jaxbContext = JAXBContext.newInstance(ArrayList.class);
        @Nullable final Marshaller marshaller = jaxbContext.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        marshaller.marshal(new ArrayList<>(entities), path);
    }

    @Nullable
    @Override
    public List<Task> loadBin() throws Exception {
        @Nullable final File file = new File(General.SAVE_PATH + General.TASKS_BIN);
        try (@Nullable final ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream(file))) {
            @Nullable final ArrayList<Task> tasks = (ArrayList) objectInputStream.readObject();
            return tasks;
        }
    }

    @Nullable
    @Override
    public List<Task> loadFasterxmlJson() throws Exception {
        @Nullable final File file = new File(General.SAVE_PATH + General.TASKS_JSON);
        @Nullable final ObjectMapper objectMapper = new ObjectMapper();
        @Nullable final ArrayList<Task> tasks = objectMapper.readValue(file, ArrayList.class);
        return tasks;
    }

    @Nullable
    @Override
    public List<Task> loadFasterxmlXml() throws Exception {
        @Nullable final File file = new File(General.SAVE_PATH + General.TASKS_XML);
        @Nullable final XmlMapper xmlMapper = new XmlMapper();
        @Nullable final ArrayList<Task> tasks = xmlMapper.readValue(file, ArrayList.class);
        return tasks;
    }

    @Nullable
    @Override
    public List<Task> loadJaxbJson() throws Exception {
        @Nullable final File file = new File(General.SAVE_PATH + General.TASKS_JSON);
        @Nullable final JAXBContext jaxbContext = JAXBContext.newInstance(ArrayList.class);
        @Nullable final Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
        @Nullable final ArrayList<Task> tasks = (ArrayList<Task>) unmarshaller.unmarshal(file);
        return tasks;
    }

    @Nullable
    @Override
    public List<Task> loadJaxbXml() throws Exception {
        @Nullable final File file = new File(General.SAVE_PATH + General.TASKS_XML);
        @Nullable final JAXBContext jaxbContext = JAXBContext.newInstance(ArrayList.class);
        @Nullable final Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
        @Nullable final ArrayList<Task> tasks = (ArrayList<Task>) unmarshaller.unmarshal(file);
        return tasks;
    }

    /*@Nullable
    private Task fetch(@Nullable final ResultSet row) {
        if (row == null) return null;
        @NotNull final Task task = new Task();
        task.setId(row.getString(DbConstant.ID));
        task.setDateBegin(row.getDate(DbConstant.DATE_BEGIN));
        task.setDateEnd(row.getDate(DbConstant.DATE_END));
        task.setName(row.getString(DbConstant.NAME));
        task.setDescription(row.getString(DbConstant.DESCRIPTION));
        task.setUserId(row.getString(DbConstant.USER_ID));
        task.setProjectId(row.getString(DbConstant.PROJECT_ID));
        return task;
    }*/

}
