package com.dolnikova.tm.util;

import com.dolnikova.tm.constant.General;
import com.dolnikova.tm.entity.Project;
import com.dolnikova.tm.entity.Session;
import com.dolnikova.tm.entity.Task;
import com.dolnikova.tm.entity.User;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Environment;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.enterprise.inject.Produces;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.io.FileInputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

public class HibernateUtil {

    private HibernateUtil() {
    }

    @NotNull
    @Produces
    public static EntityManagerFactory factory() {
        @Nullable String driver = "";
        @Nullable String url = "";
        @Nullable String username = "";
        @Nullable String password = "";
        try {
            @NotNull final Properties props = new Properties();
            @Nullable final FileInputStream in = new FileInputStream(General.PROPERTIES_PATH);
            props.load(in);
            in.close();
            driver = props.getProperty("jdbc.driver");
            if (driver != null) Class.forName(driver);
            url = props.getProperty("jdbc.url");
            username = props.getProperty("jdbc.username");
            password = props.getProperty("jdbc.password");
        } catch (Exception e) {e.printStackTrace();}
        @NotNull final Map<String, String> settings = new HashMap<>();
        settings.put(Environment.DRIVER, driver);
        settings.put(Environment.URL, url);
        settings.put(Environment.USER, username);
        settings.put(Environment.PASS, password);
        settings.put(Environment.DIALECT, "org.hibernate.dialect.MySQL5InnoDBDialect");
        settings.put(Environment.HBM2DDL_AUTO, "update");
        settings.put(Environment.SHOW_SQL, "true");

        @NotNull final StandardServiceRegistryBuilder registryBuilder = new StandardServiceRegistryBuilder();
        registryBuilder.applySettings(settings);
        @NotNull final StandardServiceRegistry registry = registryBuilder.build();

        @NotNull final MetadataSources sources = new MetadataSources(registry);
        sources.addAnnotatedClass(Task.class);
        sources.addAnnotatedClass(Project.class);
        sources.addAnnotatedClass(User.class);
        sources.addAnnotatedClass(Session.class);

        @NotNull final Metadata metadata = sources.getMetadataBuilder().build();
        return metadata.getSessionFactoryBuilder().build();
    }

    @NotNull
    @Produces
    public static EntityManager entityManagerFactory() {
        @Nullable String driver = "";
        @Nullable String url = "";
        @Nullable String username = "";
        @Nullable String password = "";
        try {
            @NotNull final Properties props = new Properties();
            @Nullable final FileInputStream in = new FileInputStream(General.PROPERTIES_PATH);
            props.load(in);
            in.close();
            driver = props.getProperty("jdbc.driver");
            if (driver != null) Class.forName(driver);
            url = props.getProperty("jdbc.url");
            username = props.getProperty("jdbc.username");
            password = props.getProperty("jdbc.password");
        } catch (Exception e) {e.printStackTrace();}
        @NotNull final Map<String, String> settings = new HashMap<>();
        settings.put(Environment.DRIVER, driver);
        settings.put(Environment.URL, url);
        settings.put(Environment.USER, username);
        settings.put(Environment.PASS, password);
        settings.put(Environment.DIALECT, "org.hibernate.dialect.MySQL5InnoDBDialect");
        settings.put(Environment.HBM2DDL_AUTO, "update");
        settings.put(Environment.SHOW_SQL, "true");

        @NotNull final StandardServiceRegistryBuilder registryBuilder = new StandardServiceRegistryBuilder();
        registryBuilder.applySettings(settings);
        @NotNull final StandardServiceRegistry registry = registryBuilder.build();

        @NotNull final MetadataSources sources = new MetadataSources(registry);
        sources.addAnnotatedClass(Task.class);
        sources.addAnnotatedClass(Project.class);
        sources.addAnnotatedClass(User.class);
        sources.addAnnotatedClass(Session.class);

        @NotNull final Metadata metadata = sources.getMetadataBuilder().build();
        return metadata.getSessionFactoryBuilder().build().createEntityManager();
    }

}
