package com.dolnikova.tm.util;

import com.dolnikova.tm.constant.General;

import java.io.FileInputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Properties;

public  class DatabaseUtil {

    public static Connection getConnection() {
        try {
            Properties props = new Properties();
            FileInputStream in = new FileInputStream(General.PROPERTIES_PATH);
            props.load(in);
            in.close();
            String driver = props.getProperty("jdbc.driver");
            if (driver != null) {
                Class.forName(driver);
            }
            String url = props.getProperty("jdbc.url");
            String username = props.getProperty("jdbc.username");
            String password = props.getProperty("jdbc.password");

            return DriverManager.getConnection(url, username, password);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

}
