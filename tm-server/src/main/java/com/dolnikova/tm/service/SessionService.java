package com.dolnikova.tm.service;

import com.dolnikova.tm.api.service.ISessionService;
import com.dolnikova.tm.entity.Session;
import com.dolnikova.tm.entity.User;
import com.dolnikova.tm.repository.SessionRepository;
import com.dolnikova.tm.util.SignatureUtil;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.persistence.EntityManager;
import java.util.List;
import java.util.logging.Logger;

@NoArgsConstructor
public final class SessionService extends AbstractService<Session> implements ISessionService {

    private final Logger LOGGER = Logger.getLogger(SessionService.class.getName());

    @Override
    public void createSession(@Nullable final User user) {
        LOGGER.info("[Создание сессии для пользователя]");
        if (user == null) return;
        @NotNull final Session session = new Session();
        session.setUser(user);
        @Nullable final String signature = SignatureUtil.sign(user.getPasswordHash(), "JAVA", 3);
        session.setSignature(signature);
        persist(session);
    }

    @Nullable
    @Override
    public Session findOneById(@Nullable final String userId, @Nullable final String id) {
        LOGGER.info("[Поиск сессии по id]");
        if (userId == null || userId.isEmpty()) return null;
        if (id == null || id.isEmpty()) return null;
        @NotNull final EntityManager em = emf.createEntityManager();
        @NotNull final SessionRepository sessionRepository = new SessionRepository(em);
        try {
            return sessionRepository.findOneById(userId, id);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Nullable
    @Override
    public Session findOneByUserId(@Nullable final String userId) {
        LOGGER.info("[Поиск сессии по id пользователя]");
        if (userId == null || userId.isEmpty()) return null;
        @NotNull final EntityManager em = emf.createEntityManager();
        @NotNull final SessionRepository sessionRepository = new SessionRepository(em);
        try {
            return sessionRepository.findOneByUserId(userId);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Nullable
    public Session findOneBySignature(@Nullable final String signature) {
        LOGGER.info("[Поиск сессии по сигнатуре]");
        if (signature == null || signature.isEmpty()) return null;
        @NotNull final EntityManager em = emf.createEntityManager();
        @NotNull final SessionRepository sessionRepository = new SessionRepository(em);
        try {
            return sessionRepository.findOneBySignature(signature);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public @Nullable List<Session> findAll(@Nullable final String userId) {
        LOGGER.info("[Поиск всех сессий]");
        if (userId == null || userId.isEmpty()) return null;
        @NotNull final EntityManager em = emf.createEntityManager();
        @NotNull final SessionRepository sessionRepository = new SessionRepository(em);
        try {
            return sessionRepository.findAll(userId);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void persist(@Nullable final Session session) {
        LOGGER.info("[Вставка сессии]");
        if (session == null) return;
        @NotNull final EntityManager em = emf.createEntityManager();
        @NotNull final SessionRepository sessionRepository = new SessionRepository(em);
        em.getTransaction().begin();
        try {
            sessionRepository.persist(session);
        } catch (Exception e) {
            e.printStackTrace();
            em.getTransaction().rollback();
        }
        em.getTransaction().commit();
    }

    @Override
    public void persistList(@Nullable List<Session> list) {
        if (list == null || list.isEmpty()) return;
        for (Session session : list) persist(session);
    }

    @Override
    public void remove(@Nullable final Session session) {
        LOGGER.info("[Удаление сессии]");
        if (session == null) return;
        @NotNull final EntityManager em = emf.createEntityManager();
        @NotNull final SessionRepository sessionRepository = new SessionRepository(em);
        @Nullable final Session dbSession = em.find(Session.class, session.getId());
        if (dbSession == null) return;
        em.getTransaction().begin();
        try {
            sessionRepository.remove(dbSession);
        } catch (Exception e) {
            e.printStackTrace();
            em.getTransaction().rollback();
        }
        em.getTransaction().commit();
    }

    @Override
    public void removeAll(@Nullable final String userId) {
        LOGGER.info("[Удаление всех проектов пользователя]");
        if (userId == null || userId.isEmpty()) return;
        @Nullable final List<Session> sessions = findAll(userId);
        if (sessions == null || sessions.isEmpty()) return;
        for (Session session : sessions) {
            remove(session);
        }
    }

    @Override
    public void saveBin(@Nullable List<Session> entities) throws Exception {
        if (entities == null || entities.isEmpty()) return;
        @NotNull final EntityManager em = emf.createEntityManager();
        @NotNull final SessionRepository sessionRepository = new SessionRepository(em);
        sessionRepository.saveBin(entities);
    }

    @Override
    public void saveFasterxmlJson(@Nullable List<Session> entities) throws Exception {
        if (entities == null || entities.isEmpty()) return;
        @NotNull final EntityManager em = emf.createEntityManager();
        @NotNull final SessionRepository sessionRepository = new SessionRepository(em);
        sessionRepository.saveFasterxmlJson(entities);
    }

    @Override
    public void saveFasterxmlXml(@Nullable List<Session> entities) throws Exception {
        if (entities == null || entities.isEmpty()) return;
        @NotNull final EntityManager em = emf.createEntityManager();
        @NotNull final SessionRepository sessionRepository = new SessionRepository(em);
        sessionRepository.saveFasterxmlXml(entities);
    }

    @Override
    public void saveJaxbJson(@Nullable List<Session> entities) throws Exception {
        if (entities == null || entities.isEmpty()) return;
        @NotNull final EntityManager em = emf.createEntityManager();
        @NotNull final SessionRepository sessionRepository = new SessionRepository(em);
        sessionRepository.saveJaxbJson(entities);
    }

    @Override
    public void saveJaxbXml(@Nullable List<Session> entities) throws Exception {
        if (entities == null || entities.isEmpty()) return;
        @NotNull final EntityManager em = emf.createEntityManager();
        @NotNull final SessionRepository sessionRepository = new SessionRepository(em);
        sessionRepository.saveJaxbXml(entities);
    }

    @Override
    public @Nullable List<Session> loadBin() throws Exception {
        @NotNull final EntityManager em = emf.createEntityManager();
        @NotNull final SessionRepository sessionRepository = new SessionRepository(em);
        return sessionRepository.loadBin();
    }

    @Override
    public @Nullable List<Session> loadFasterxmlJson() throws Exception {
        @NotNull final EntityManager em = emf.createEntityManager();
        @NotNull final SessionRepository sessionRepository = new SessionRepository(em);
        return sessionRepository.loadFasterxmlJson();
    }

    @Override
    public @Nullable List<Session> loadFasterxmlXml() throws Exception {
        @NotNull final EntityManager em = emf.createEntityManager();
        @NotNull final SessionRepository sessionRepository = new SessionRepository(em);
        return sessionRepository.loadFasterxmlXml();
    }

    @Override
    public @Nullable List<Session> loadJaxbJson() throws Exception {
        @NotNull final EntityManager em = emf.createEntityManager();
        @NotNull final SessionRepository sessionRepository = new SessionRepository(em);
        return sessionRepository.loadJaxbJson();
    }

    @Override
    public @Nullable List<Session> loadJaxbXml() throws Exception {
        @NotNull final EntityManager em = emf.createEntityManager();
        @NotNull final SessionRepository sessionRepository = new SessionRepository(em);
        return sessionRepository.loadJaxbXml();
    }

}
