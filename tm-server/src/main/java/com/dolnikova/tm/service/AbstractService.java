package com.dolnikova.tm.service;

import com.dolnikova.tm.api.service.IService;
import com.dolnikova.tm.entity.AbstractEntity;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;

import javax.inject.Inject;
import javax.persistence.EntityManagerFactory;
import java.util.List;

@NoArgsConstructor
public abstract class AbstractService<E extends AbstractEntity> implements IService<E> {

    @Inject
    EntityManagerFactory emf;

    @Override
    public void saveBin(@Nullable List<E> entities) throws Exception {
        if (entities == null || entities.isEmpty()) return;
    }

    @Override
    public void saveFasterxmlJson(@Nullable List<E> entities) throws Exception {
        if (entities == null || entities.isEmpty()) return;
    }

    @Override
    public void saveFasterxmlXml(@Nullable List<E> entities) throws Exception {
        if (entities == null || entities.isEmpty()) return;
    }

    @Override
    public void saveJaxbJson(@Nullable List<E> entities) throws Exception {
        if (entities == null || entities.isEmpty()) return;
    }

    @Override
    public void saveJaxbXml(@Nullable List<E> entities) throws Exception {
        if (entities == null || entities.isEmpty()) return;
    }

    @Override
    public @Nullable List<E> loadBin() throws Exception {
        return null;
    }

    @Override
    public @Nullable List<E> loadFasterxmlJson() throws Exception {
        return null;
    }

    @Override
    public @Nullable List<E> loadFasterxmlXml() throws Exception {
        return null;
    }

    @Override
    public @Nullable List<E> loadJaxbJson() throws Exception {
        return null;
    }

    @Override
    public @Nullable List<E> loadJaxbXml() throws Exception {
        return null;
    }
}
