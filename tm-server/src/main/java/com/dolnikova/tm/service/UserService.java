package com.dolnikova.tm.service;

import com.dolnikova.tm.api.service.IUserService;
import com.dolnikova.tm.entity.Session;
import com.dolnikova.tm.entity.User;
import com.dolnikova.tm.enumerated.DataType;
import com.dolnikova.tm.repository.UserRepository;
import com.dolnikova.tm.util.PasswordHashUtil;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import javax.validation.constraints.Null;
import java.util.List;
import java.util.logging.Logger;

@NoArgsConstructor
public final class UserService extends AbstractService<User> implements IUserService {

    private final Logger LOGGER = Logger.getLogger(UserService.class.getName());

    @Nullable
    @Transactional
    public User findOneById(@Nullable final String id) {
        LOGGER.info("[Поиск пользователя по id]");
        if (id == null || id.isEmpty()) return null;
        @NotNull final EntityManager em = emf.createEntityManager();
        @NotNull final UserRepository userRepository = new UserRepository(em);
        try {
            em.getTransaction().begin();
            @Nullable final User user = userRepository.findOneById(id);
            em.getTransaction().commit();
            return user;
        } catch (Exception e) {
            e.printStackTrace();
            em.getTransaction().rollback();
        }
        return null;
    }

    @Override
    @Transactional
    public User findOneByLogin(@Nullable final String login) {
        LOGGER.info("[Поиск пользователя по логину " + login + " ]");
        if (login == null || login.isEmpty()) return null;
        @NotNull final EntityManager em = emf.createEntityManager();
        @NotNull final UserRepository userRepository = new UserRepository(em);
        try {
            em.getTransaction().begin();
            @Nullable final User user = userRepository.findOneByLogin(login);
            em.getTransaction().commit();
            return user;
        } catch (Exception e) {
            e.printStackTrace();
            em.getTransaction().rollback();
        }
        return null;
    }

    @Override
    public User findOneBySession(@Nullable Session session) {
        LOGGER.info("[Поиск пользователя по сессии]");
        if (session == null) return null;
        @Nullable final String userId = session.getUser().getId();
        if (userId == null || userId.isEmpty()) return null;
        return findOneById(userId);
    }

    @Override
    public @Nullable List<User> findAll(@Nullable final String userId) {
        LOGGER.info("[Поиск всех пользователей]");
        if (userId == null || userId.isEmpty()) return null;
        @NotNull final EntityManager em = emf.createEntityManager();
        @NotNull final UserRepository userRepository = new UserRepository(em);
        try {
            return userRepository.findAll(userId);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public @Nullable List<User> findAllByLogin(@Nullable final String ownerId, @Nullable final String login) {
        LOGGER.info("[Поиск всех пользователей по логину " + login + " ]");
        if (ownerId == null || ownerId.isEmpty()) return null;
        if (login == null || login.isEmpty()) return null;
        @NotNull final EntityManager em = emf.createEntityManager();
        @NotNull final UserRepository userRepository = new UserRepository(em);
        try {
            return userRepository.findAllByLogin(ownerId, login);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void persist(@Nullable final User user) {
        LOGGER.info("[Вставка пользователя]");
        if (user == null) return;
        @NotNull final EntityManager em = emf.createEntityManager();
        @NotNull final UserRepository userRepository = new UserRepository(em);
        em.getTransaction().begin();
        try {
            userRepository.persist(user);
        } catch (Exception e) {
            e.printStackTrace();
            em.getTransaction().rollback();
        }
        em.getTransaction().commit();
    }

    @Override
    public void persistList(@Nullable List<User> list) {
        if (list == null || list.isEmpty()) return;
        for (User user : list) persist(user);
    }

    @Override
    public void merge(@Nullable final String newData,
                      @Nullable final User entityToMerge,
                      @Nullable final DataType dataType) {
        LOGGER.info("[Изменение данных пользователя]");
        if (newData == null || newData.isEmpty()) return;
        if (entityToMerge == null || dataType == null) return;
        @Nullable final User dbUser = findOneById(entityToMerge.getId());
        if (dbUser == null) return;
        @NotNull final EntityManager em = emf.createEntityManager();
        @NotNull final UserRepository userRepository = new UserRepository(em);
        try {
            em.getTransaction().begin();
            userRepository.merge(newData, dbUser, dataType);
        } catch (Exception e) {
            e.printStackTrace();
            em.getTransaction().rollback();
        }
        em.getTransaction().commit();

    }

    @Override
    public void remove(@Nullable final User user) {
        LOGGER.info("[Удаление пользователя]");
        if (user == null) return;
        @NotNull final EntityManager em = emf.createEntityManager();
        @NotNull final UserRepository userRepository = new UserRepository(em);
        @Nullable final User dbUser = em.find(User.class, user.getId());
        if (dbUser == null) return;
        try {
            em.getTransaction().begin();
            userRepository.remove(dbUser);
        } catch (Exception e) {
            e.printStackTrace();
            em.getTransaction().rollback();
        }
        em.getTransaction().commit();
    }

    @Override
    public void removeAll(@Nullable final String ownerId) {
        LOGGER.info("[Удаление всех пользователей]");
        if (ownerId == null || ownerId.isEmpty()) return;
        @Nullable final List<User> users = findAll(ownerId);
        if (users == null || users.isEmpty()) return;
        for (User user : users) {
            remove(user);
        }
    }

    @Override
    public boolean checkPassword(@Nullable String userId, @Nullable String userInput) {
        if (userId == null || userId.isEmpty()) return false;
        if (userInput == null || userInput.isEmpty()) return false;
        @Nullable final String hashedInput = PasswordHashUtil.md5(userInput);
        if (hashedInput == null || hashedInput.isEmpty()) return false;
        @Nullable final User user = findOneById(userId);
        if (user != null) {
            @Nullable final String password = user.getPasswordHash();
            return hashedInput.equals(password);
        }
        return false;
    }

    @Override
    public void saveBin(@Nullable List<User> entities) throws Exception {
        if (entities == null || entities.isEmpty()) return;
        @NotNull final EntityManager em = emf.createEntityManager();
        @NotNull final UserRepository userRepository = new UserRepository(em);
        userRepository.saveBin(entities);
    }

    @Override
    public void saveFasterxmlJson(@Nullable List<User> entities) throws Exception {
        if (entities == null || entities.isEmpty()) return;
        @NotNull final EntityManager em = emf.createEntityManager();
        @NotNull final UserRepository userRepository = new UserRepository(em);
        userRepository.saveFasterxmlJson(entities);
    }

    @Override
    public void saveFasterxmlXml(@Nullable List<User> entities) throws Exception {
        if (entities == null || entities.isEmpty()) return;
        @NotNull final EntityManager em = emf.createEntityManager();
        @NotNull final UserRepository userRepository = new UserRepository(em);
        userRepository.saveFasterxmlXml(entities);
    }

    @Override
    public void saveJaxbJson(@Nullable List<User> entities) throws Exception {
        if (entities == null || entities.isEmpty()) return;
        @NotNull final EntityManager em = emf.createEntityManager();
        @NotNull final UserRepository userRepository = new UserRepository(em);
        userRepository.saveJaxbJson(entities);
    }

    @Override
    public void saveJaxbXml(@Nullable List<User> entities) throws Exception {
        if (entities == null || entities.isEmpty()) return;
        @NotNull final EntityManager em = emf.createEntityManager();
        @NotNull final UserRepository userRepository = new UserRepository(em);
        userRepository.saveJaxbXml(entities);
    }

    @Override
    public @Nullable List<User> loadBin() throws Exception {
        @NotNull final EntityManager em = emf.createEntityManager();
        @NotNull final UserRepository userRepository = new UserRepository(em);
        return userRepository.loadBin();
    }

    @Override
    public @Nullable List<User> loadFasterxmlJson() throws Exception {
        @NotNull final EntityManager em = emf.createEntityManager();
        @NotNull final UserRepository userRepository = new UserRepository(em);
        return userRepository.loadFasterxmlJson();
    }

    @Override
    public @Nullable List<User> loadFasterxmlXml() throws Exception {
        @NotNull final EntityManager em = emf.createEntityManager();
        @NotNull final UserRepository userRepository = new UserRepository(em);
        return userRepository.loadFasterxmlXml();
    }

    @Override
    public @Nullable List<User> loadJaxbJson() throws Exception {
        @NotNull final EntityManager em = emf.createEntityManager();
        @NotNull final UserRepository userRepository = new UserRepository(em);
        return userRepository.loadJaxbJson();
    }

    @Override
    public @Nullable List<User> loadJaxbXml() throws Exception {
        @NotNull final EntityManager em = emf.createEntityManager();
        @NotNull final UserRepository userRepository = new UserRepository(em);
        return userRepository.loadJaxbXml();
    }

}
