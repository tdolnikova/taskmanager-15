package com.dolnikova.tm.service;

import com.dolnikova.tm.api.service.ITaskService;
import com.dolnikova.tm.entity.Task;
import com.dolnikova.tm.enumerated.DataType;
import com.dolnikova.tm.repository.TaskRepository;
import com.dolnikova.tm.util.HibernateUtil;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.util.List;
import java.util.logging.Logger;

@NoArgsConstructor
public final class TaskService extends AbstractService<Task> implements ITaskService {

    private final Logger LOGGER = Logger.getLogger(TaskService.class.getName());

    @Nullable
    @Override
    public Task findOneById(@Nullable final String userId, @Nullable final String id) {
        LOGGER.info("[Поиск задачи по id]");
        if (userId == null || userId.isEmpty()) return null;
        if (id == null || id.isEmpty()) return null;
        @NotNull final EntityManager em = emf.createEntityManager();
        @NotNull final TaskRepository taskRepository = new TaskRepository(em);
        try {
            return taskRepository.findOneById(userId, id);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Task findOneByName(@Nullable final String name) {
        LOGGER.info("[Поиск задачи по названию]");
        if (name == null || name.isEmpty()) return null;
        @NotNull final EntityManager em = emf.createEntityManager();
        @NotNull final TaskRepository taskRepository = new TaskRepository(em);
        try {
            return taskRepository.findOneByName(name);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public @Nullable List<Task> findAll(@Nullable final String userId) {
        LOGGER.info("[Поиск всех задач]");
        if (userId == null || userId.isEmpty()) return null;
        @NotNull final EntityManager em = emf.createEntityManager();
        @NotNull final TaskRepository taskRepository = new TaskRepository(em);
        try {
            return taskRepository.findAll(userId);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public @Nullable List<Task> findAllByName(@Nullable final String userId, @Nullable final String text) {
        LOGGER.info("[Поиск всех задач по названию]");
        if (userId == null || userId.isEmpty()) return null;
        if (text == null || text.isEmpty()) return null;
        @NotNull final EntityManager em = emf.createEntityManager();
        @NotNull final TaskRepository taskRepository = new TaskRepository(em);
        try {
            return taskRepository.findAllByName(userId, text);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public @Nullable List<Task> findAllByDescription(@Nullable final String userId, @Nullable final String text) {
        LOGGER.info("[Поиск всех задач по описанию]");
        if (userId == null || userId.isEmpty()) return null;
        if (text == null || text.isEmpty()) return null;
        @NotNull final EntityManager em = emf.createEntityManager();
        @NotNull final TaskRepository taskRepository = new TaskRepository(em);
        try {
            return taskRepository.findAllByDescription(userId, text);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void persist(@Nullable final Task task) {
        LOGGER.info("[Вставка задачи]");
        if (task == null) return;
        @NotNull final EntityManager em = emf.createEntityManager();
        @NotNull final TaskRepository taskRepository = new TaskRepository(em);
        em.getTransaction().begin();
        try {
            taskRepository.persist(task);
        } catch (Exception e) {
            e.printStackTrace();
            em.getTransaction().rollback();
        }
        em.getTransaction().commit();
    }

    @Override
    public void persistList(@Nullable List<Task> list) {
        if (list == null || list.isEmpty()) return;
        for (Task task : list) persist(task);
    }

    @Override
    public void merge(@Nullable final String newData,
                      @Nullable final Task entityToMerge,
                      @Nullable final DataType dataType) {
        LOGGER.info("[Изменение данных задачи]");
        if (newData == null || newData.isEmpty()) return;
        if (entityToMerge == null || dataType == null) return;
        @NotNull final EntityManager em = emf.createEntityManager();
        @NotNull final TaskRepository taskRepository = new TaskRepository(em);
        @Nullable final Task dbTask = em.find(Task.class, entityToMerge.getId());
        if (dbTask == null) return;
        em.getTransaction().begin();
        try {
            taskRepository.merge(newData, dbTask, dataType);
        } catch (Exception e) {
            em.getTransaction().rollback();
            e.printStackTrace();
        }
        em.getTransaction().commit();
    }

    @Override
    public void remove(@Nullable final Task entity) {
        if (entity == null) return;
        LOGGER.info("[Удаление задачи " + entity.getId() +"]");
        @NotNull final EntityManager em = emf.createEntityManager();
        @NotNull final TaskRepository taskRepository = new TaskRepository(em);
        Task dbTask = em.find(Task.class, entity.getId());
        if (dbTask == null) return;
        em.getTransaction().begin();
        try {
            taskRepository.remove(dbTask);
        } catch (Exception e) {
            em.getTransaction().rollback();
            e.printStackTrace();
        }
        em.getTransaction().commit();
    }

    @Override
    public void removeAll(@Nullable final String userId) {
        LOGGER.info("[Удаление всех задач пользователя]");
        if (userId == null || userId.isEmpty()) return;
        @Nullable final List<Task> tasks = findAll(userId);
        if (tasks == null || tasks.isEmpty()) return;
        for (Task task : tasks) {
            remove(task);
        }
    }

    @Override
    public void saveBin(@Nullable List<Task> entities) throws Exception {
        if (entities == null || entities.isEmpty()) return;
        @NotNull final EntityManager em = emf.createEntityManager();
        @NotNull final TaskRepository taskRepository = new TaskRepository(em);
        taskRepository.saveBin(entities);
    }

    @Override
    public void saveFasterxmlJson(@Nullable List<Task> entities) throws Exception {
        if (entities == null || entities.isEmpty()) return;
        @NotNull final EntityManager em = emf.createEntityManager();
        @NotNull final TaskRepository taskRepository = new TaskRepository(em);
        taskRepository.saveFasterxmlJson(entities);
    }

    @Override
    public void saveFasterxmlXml(@Nullable List<Task> entities) throws Exception {
        if (entities == null || entities.isEmpty()) return;
        @NotNull final EntityManager em = emf.createEntityManager();
        @NotNull final TaskRepository taskRepository = new TaskRepository(em);
        taskRepository.saveFasterxmlXml(entities);
    }

    @Override
    public void saveJaxbJson(@Nullable List<Task> entities) throws Exception {
        if (entities == null || entities.isEmpty()) return;
        @NotNull final EntityManager em = emf.createEntityManager();
        @NotNull final TaskRepository taskRepository = new TaskRepository(em);
        taskRepository.saveJaxbJson(entities);
    }

    @Override
    public void saveJaxbXml(@Nullable List<Task> entities) throws Exception {
        if (entities == null || entities.isEmpty()) return;
        @NotNull final EntityManager em = emf.createEntityManager();
        @NotNull final TaskRepository taskRepository = new TaskRepository(em);
        taskRepository.saveJaxbXml(entities);
    }

    @Override
    public @Nullable List<Task> loadBin() throws Exception {
        @NotNull final EntityManager em = emf.createEntityManager();
        @NotNull final TaskRepository taskRepository = new TaskRepository(em);
        return taskRepository.loadBin();
    }

    @Override
    public @Nullable List<Task> loadFasterxmlJson() throws Exception {
        @NotNull final EntityManager em = emf.createEntityManager();
        @NotNull final TaskRepository taskRepository = new TaskRepository(em);
        return taskRepository.loadFasterxmlJson();
    }

    @Override
    public @Nullable List<Task> loadFasterxmlXml() throws Exception {
        @NotNull final EntityManager em = emf.createEntityManager();
        @NotNull final TaskRepository taskRepository = new TaskRepository(em);
        return taskRepository.loadFasterxmlXml();
    }

    @Override
    public @Nullable List<Task> loadJaxbJson() throws Exception {
        @NotNull final EntityManager em = emf.createEntityManager();
        @NotNull final TaskRepository taskRepository = new TaskRepository(em);
        return taskRepository.loadJaxbJson();
    }

    @Override
    public @Nullable List<Task> loadJaxbXml() throws Exception {
        @NotNull final EntityManager em = emf.createEntityManager();
        @NotNull final TaskRepository taskRepository = new TaskRepository(em);
        return taskRepository.loadJaxbXml();
    }

}
