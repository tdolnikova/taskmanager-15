package com.dolnikova.tm.exception;

public final class NoAuthorizationException extends Exception {

    public NoAuthorizationException() {
        super("No authorization");
    }

}