package com.dolnikova.tm;

import com.dolnikova.tm.bootstrap.Bootstrap;

import javax.enterprise.inject.se.SeContainerInitializer;

public final class ServerApp {

    public static void main(String[] args) {
        SeContainerInitializer.newInstance()
                .addPackages(ServerApp.class).initialize()
                .select(Bootstrap.class).get()
                .init();
    }

}