package com.dolnikova.tm.enumerated;

public enum DataType {
    NAME,
    DESCRIPTION,
    LOGIN,
    PASSWORD,
    ROLE
}
