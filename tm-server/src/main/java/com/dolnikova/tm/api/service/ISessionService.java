package com.dolnikova.tm.api.service;

import com.dolnikova.tm.entity.Session;
import com.dolnikova.tm.entity.User;
import com.dolnikova.tm.enumerated.DataType;
import org.jetbrains.annotations.Nullable;

import javax.validation.constraints.Null;
import java.util.List;

public interface ISessionService extends IService<Session> {

    void createSession(User user);

    @Nullable
    Session findOneById(final @Nullable String ownerId, final @Nullable String id);

    @Nullable
    Session findOneByUserId(final @Nullable String userId);

    @Nullable
    Session findOneBySignature(@Nullable final String signature);

    @Override
    @Nullable List<Session> findAll(final @Nullable String ownerId);

    @Override
    void persist(final @Nullable Session entity);

    @Override
    void persistList(final @Nullable List<Session> list);

    @Override
    void remove(final @Nullable Session entity);

    @Override
    void removeAll(final @Nullable String ownerId);

    @Override
    void saveBin(final @Nullable List<Session> entities) throws Exception;

    @Override
    void saveFasterxmlJson(final @Nullable List<Session> entities) throws Exception;

    @Override
    void saveFasterxmlXml(final @Nullable List<Session> entities) throws Exception;

    @Override
    void saveJaxbJson(final @Nullable List<Session> entities) throws Exception;

    @Override
    void saveJaxbXml(final @Nullable List<Session> entities) throws Exception;

    @Override
    @Nullable List<Session> loadBin() throws Exception;

    @Override
    @Nullable List<Session> loadFasterxmlJson() throws Exception;

    @Override
    @Nullable List<Session> loadFasterxmlXml() throws Exception;

    @Override
    @Nullable List<Session> loadJaxbJson() throws Exception;

    @Override
    @Nullable List<Session> loadJaxbXml() throws Exception;
}
