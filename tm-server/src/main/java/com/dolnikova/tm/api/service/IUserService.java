package com.dolnikova.tm.api.service;

import com.dolnikova.tm.entity.Session;
import com.dolnikova.tm.entity.User;
import com.dolnikova.tm.enumerated.DataType;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface IUserService extends IService<User> {

    @Nullable
    User findOneById(@Nullable final String id);

    @Nullable
    User findOneByLogin(final @Nullable String name);

    @Nullable
    User findOneBySession(@Nullable final Session session);

    @Override
    @Nullable List<User> findAll(final @Nullable String ownerId);

    @Nullable List<User> findAllByLogin(@Nullable String ownerId, @Nullable String login);

    @Override
    void persist(final @Nullable User entity);

    @Override
    void persistList(final @Nullable List<User> list);

    void merge(final @Nullable String newData, final @Nullable User entityToMerge, final @Nullable DataType dataType);

    @Override
    void remove(final @Nullable User entity);

    @Override
    void removeAll(final @Nullable String ownerId);

    boolean checkPassword(final @Nullable String userId, final @Nullable String userInput);

    @Override
    void saveBin(final @Nullable List<User> entities) throws Exception;

    @Override
    void saveFasterxmlJson(final @Nullable List<User> entities) throws Exception;

    @Override
    void saveFasterxmlXml(final @Nullable List<User> entities) throws Exception;

    @Override
    void saveJaxbJson(final @Nullable List<User> entities) throws Exception;

    @Override
    void saveJaxbXml(final @Nullable List<User> entities) throws Exception;

    @Override
    @Nullable List<User> loadBin() throws Exception;

    @Override
    @Nullable List<User> loadFasterxmlJson() throws Exception;

    @Override
    @Nullable List<User> loadFasterxmlXml() throws Exception;

    @Override
    @Nullable List<User> loadJaxbJson() throws Exception;

    @Override
    @Nullable List<User> loadJaxbXml() throws Exception;
}
