package com.dolnikova.tm.dto;

import com.dolnikova.tm.enumerated.Status;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public class TaskDTO extends AbstractDTO {

    @Nullable
    private Date dateBegin;

    @Nullable
    private Date dateEnd;

    @Nullable
    private String description;

    @Nullable
    private String name;

    @Nullable
    private String projectId;

    @Nullable
    private String userId;

    @Nullable
    private Date creationDate = new Date();

    @Nullable
    @Enumerated(EnumType.STRING)
    private Status status = Status.PLANNED;

}
